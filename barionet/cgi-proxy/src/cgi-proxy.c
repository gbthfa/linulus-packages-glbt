#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

int main(int argc, char **argv)
{
    struct sockaddr_un server;
    struct timeval tv;
    char buf[1024];
    int sockfd;
    int nread;
    const char *path  = getenv("PATH_INFO");
    const char *query = getenv("QUERY_STRING");

    if (argc != 2) {
        fprintf(stderr, "USAGE: %s <socket-name>\n", argv[0]);
        return -1;
    }

    sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("opening stream socket");
        return -1;
    }
    tv.tv_sec  = 5;
    tv.tv_usec = 0;

    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));

    server.sun_family = AF_UNIX;
    sprintf(server.sun_path, "%s.socket", argv[1]);

    if (connect(sockfd, (struct sockaddr *) &server, sizeof(struct sockaddr_un)) < 0) {
        close(sockfd);
        perror("connecting stream socket");
        return -1;
    }

    snprintf(buf, sizeof(buf), "PATH_INFO=%s\nQUERY_STRING=%s\n",
             path ? path : "", query ? query : "");

    if (write(sockfd, buf, strlen(buf)) < 0)
        perror("writing on stream socket");

    nread = read(sockfd, buf, sizeof(buf));
    if (nread > 0) {
        buf[MIN(nread, sizeof(buf) - 1)] = '\0';
        puts(buf);
    }

    close(sockfd);

    return 0;
}
