import subprocess

def get_option(config, path):
    try:
        ret = subprocess.check_output(['uci', 'get', config + '.' + path],
                                      stderr=subprocess.STDOUT)
        return ret.strip()
    except subprocess.CalledProcessError:
        return None
    
def set_option(config, path):
    try:
        subprocess.check_call(['uci', 'set', config + '.' + path],
                                      stderr=subprocess.STDOUT)
        subprocess.check_call(['uci', 'commit', config],
                                      stderr=subprocess.STDOUT)
        return 0
    except subprocess.CalledProcessError:
        return None
