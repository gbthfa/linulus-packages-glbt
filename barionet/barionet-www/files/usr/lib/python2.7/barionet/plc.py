import select
import timerfd
import time
import socket
import errno
import struct
import random
import urlparse
import glob
import os
import traceback


PULLUP_BASE = 104
OUTPUT_BASE = 112
INPUT_BASE  = 120
BASE_PATH   = '/sys/class/gpio/gpio'

CGI_SOCKET  = '/www/rc.cgi.socket'

W1_PATH     = '/sys/bus/w1/devices/*-*/w1_slave'

ADC_RAW_MAX = 4095          # 2^12-1
ADC_PATH    = '/sys/bus/spi/devices/spi1.0/iio:device0'

EPOLL = select.epoll()
EPOLL_EVENTS  = {}
EVENTS        = []
CURRENT_STATE = None


def isIpv6(addr):
    try:
        socket.inet_pton(socket.AF_INET6, addr)
        return True
    except socket.error:
        return False


class Event(object):
    def __init__(self):
        self._fd = None
        self._callbacks = {}

    def triggered(self):
        return True

    def sync(self, events):
        raise NotImplementedError()

    def invalidate(self):
        pass

    def fileno(self):
        return self._fd

    def register(self, epoll):
        pass

    def unregister(self):
        pass

    def register_callback(self, callback, *args):
        empty = bool(self._callbacks)
        self._callbacks[callback] = args
        if not empty:
            register_event(self)

    def unregister_callback(self, callback):
        del self._callbacks[callback]
        if not self._callbacks:
            unregister_event(self)

    def dispatch_callbacks(self):
        for callback in self._callbacks.keys():
            callback(*self._callbacks[callback])

    def depends(self):
        return set([self])

    def __nonzero__(self):
        raise NotImplementedError()

    def __and__(self, other):
        return And(self, other)

    def __or__(self, other):
        return Or(self, other)

    def __invert__(self):
        return Not(self)


class BoolEvent(Event):
    def triggered(self):
        return any(child.triggered() for child in self._children)

    def invalidate(self):
        for child in self._children:
            child.invalidate()

    def depends(self):
        deps = set()
        for child in self._children:
            deps |= child.depends()
        return deps


class And(BoolEvent):
    def __init__(self, *args):
        super(And, self).__init__()
        self._children = list(args)

    def __and__(self, other):
        self._children.append(other)
        return self

    def __nonzero__(self):
        return all(child for child in self._children)

    def __str__(self):
        return '(%s)' % ' & '.join([str(child) for child in self._children])


class Or(BoolEvent):
    def __init__(self, *args):
        super(Or, self).__init__()
        self._children = list(args)

    def __or__(self, other):
        self._children.append(other)
        return self

    def __nonzero__(self):
        return any(child for child in self._children)

    def __str__(self):
        return '(%s)' % ' | '.join([str(child) for child in self._children])


class Not(Event):
    def __init__(self, obj):
        super(Not, self).__init__()
        self._child = obj

    def __nonzero__(self):
        return not self._child

    def triggered(self):
        return self._child.triggered()

    def invalidate(self):
        self._child.invalidate()

    def depends(self):
        return self._child.depends()

    def __str__(self):
        return '~%s' % self._child


class PingTimeout(Event):
    ICMP_ECHO_REQUEST = 8 # Seems to be the same on Solaris.
    ICMPV6_ECHO_REQUEST = 128

    ICMP_CODE = socket.getprotobyname('icmp')
    ICMPV6_CODE = socket.getprotobyname('ipv6-icmp')
    ERROR_DESCR = {
        1: ' - Note that ICMP messages can only be '
        'sent from processes running as root.',
        10013: ' - Note that ICMP messages can only be sent by'
        ' users or processes with administrator rights.'
    }

    def __init__(self, interval, timeout, host):
        super(PingTimeout, self).__init__()
        self._interval = interval
        self._timeout  = timeout
        self._host     = host
        self._interval_timer = Timer()
        self._interval_timer.register_callback(self.ping)
        self._timeout_timer = Timer()
        self._timeout_timer.stop() # revisit: refactor to be able to pass running to constructor
        self._timeout_timer.register_callback(self._timer_timeout)
        self._valid = False
        self._done  = False
        self.ping()

    def depends(self):
        return set([self, self._timeout_timer])

    def register(self, epoll):
        epoll.register(self._socket, select.EPOLLIN)

    def invalidate(self):
        unregister(self)
        self._socket.close()
        self._fd = None
        self._valid = False

    def shutdown(self):
        self._done = True
        self._timeout_timer.stop()
        self._interval_timer.start(self._interval)

    def _timer_timeout(self):
        self._valid = True
        self.shutdown()

    def sync(self, event):
        if self._done:
            return

        rec_packet, addr = self._socket.recvfrom(1024)
        if isIpv6(self._host):
            icmp_header = rec_packet[0:8]
        else:
            icmp_header = rec_packet[20:28]
        type, code, checksum, p_id, sequence = struct.unpack(
            'bbHHh', icmp_header)

        self._valid = p_id != self._packet_id
        self.shutdown()

    def __nonzero__(self):
        return self._valid

    def ping(self):
        self._timeout_timer.start(self._timeout)
        self._interval_timer.stop()
        self._done = False
        try:
            if isIpv6(self._host):
                self._socket = socket.socket(socket.AF_INET6, socket.SOCK_RAW,
                                             self.ICMPV6_CODE)
            else:
                self._socket = socket.socket(socket.AF_INET, socket.SOCK_RAW,
                                             self.ICMP_CODE)
            self._fd = self._socket.fileno()
        except socket.error as e:
            if e.errno in self.ERROR_DESCR:
                # Operation not permitted
                raise socket.error(''.join((e.args[1],
                                            self.ERROR_DESCR[e.errno])))
            raise # raise the original error

        register(self)

        # Maximum for an unsigned short int c object counts to 65535 so
        # we have to sure that our packet id is not greater than that.
        self._packet_id = int((id(self._timeout) * random.random()) % 65535)
        packet = self.create_packet(self._packet_id)
        while packet:
            # The icmp protocol does not use a port, but the function
            # below expects it, so we just give it a dummy port.
            if isIpv6(self._host):
                sent = self._socket.sendto(packet, (self._host, 58))
            else:
                sent = self._socket.sendto(packet, (self._host, 1))
            packet = packet[sent:]

    def checksum(self, source_string):
        # I'm not too confident that this is right but testing seems to
        # suggest that it gives the same answers as in_cksum in ping.c.
        sum = 0
        count_to = (len(source_string) / 2) * 2
        count = 0
        while count < count_to:
            this_val = ord(source_string[count + 1])*256+ord(source_string[count])
            sum = sum + this_val
            sum = sum & 0xffffffff # Necessary?
            count = count + 2
        if count_to < len(source_string):
            sum = sum + ord(source_string[len(source_string) - 1])
            sum = sum & 0xffffffff # Necessary?
        sum = (sum >> 16) + (sum & 0xffff)
        sum = sum + (sum >> 16)
        answer = ~sum
        answer = answer & 0xffff
        # Swap bytes. Bugger me if I know why.
        answer = answer >> 8 | (answer << 8 & 0xff00)
        return answer


    def create_packet(self, id):
        """Create a new echo request packet based on the given "id"."""
        # Header is type (8), code (8), checksum (16), id (16), sequence (16)
        if isIpv6(self._host):
            header = struct.pack('!BbHHh', self.ICMPV6_ECHO_REQUEST, 0, 0, id, 1)
        else:
            header = struct.pack('bbHHh', self.ICMP_ECHO_REQUEST, 0, 0, id, 1)
        data = 192 * 'Q'
        # Calculate the checksum on the data and the dummy header.
        my_checksum = self.checksum(header + data)
        # Now that we have the right checksum, we put that in. It's just easier
        # to make up a new header than to stuff it into the dummy.
        if isIpv6(self._host):
            header = struct.pack('!BbHHh', self.ICMPV6_ECHO_REQUEST, 0,
                                 socket.htons(my_checksum), id, 1)
        else:
            header = struct.pack('bbHHh', self.ICMP_ECHO_REQUEST, 0,
                                 socket.htons(my_checksum), id, 1)
        return header + data


class Timer(Event):
    def __init__(self, interval=None, initial=None, start_on=None, stop_on=None):
        super(Timer, self).__init__()
        self._fd = timerfd.create(timerfd.CLOCK_MONOTONIC, 0)
        self._expired = False
        self._enabled = False
        register(self)

        if start_on is not None:
            start_on.register_callback(self.start, interval, initial)

        if stop_on is not None:
            stop_on.register_callback(self.stop)

        if start_on is None:
            self.start(interval, initial)
        elif start_on:
            self.start(interval, initial)

    def register(self, epoll):
        epoll.register(self._fd, select.EPOLLIN | select.EPOLLET)

    def sync(self, events):
        os.read(self._fd, 8)
        self._expired = True

    def invalidate(self):
        self._expired = False

    def start(self, interval, initial=None):
        timerfd.settime(self._fd, 0, timerfd.itimerspec(interval, initial or interval))

    def stop(self):
        timerfd.settime(self._fd, 0, timerfd.itimerspec(0, 0))
        self._enabled = False

    def unregister(self):
        os.close(self._fd)
        self._fd = None

    def __nonzero__(self):
        return self._expired

    def __str__(self):
        return 'Timer(...)'


class IO(Event):
    def __init__(self, path, pin, name):
        super(IO, self).__init__()
        self._pin   = pin
        self._name  = name
        self._value = None
        self._raw   = None
        self._fd    = os.open(path, os.O_RDWR | os.O_NONBLOCK)
        self.load()

    def parse_value(self, raw):
        return raw != '0'

    def load(self):
        self._raw = os.read(self._fd, 16).strip()
        self._value = self.parse_value(self._raw)
        return self._value

    def sync(self, events=None):
        os.lseek(self._fd, 0, os.SEEK_SET)
        self.load()

    @property
    def pin(self):
        return self._pin
    
    @property
    def name(self):
        return self._name

    def __nonzero__(self):
        return self._value

    @property
    def fd(self):
        return self._fd


class Output(IO):
    def __init__(self, pin, name, set_to=None):
        super(Output, self).__init__(
            '%s%d/value' % (BASE_PATH, pin), pin, name)        
        self._timer = Timer()
        self._timer.register_callback(self._pulse_reset)
        
        if set_to != None:
            self.set(set_to)

    def set(self, on=True):
        os.lseek(self._fd, 0, os.SEEK_SET)
        os.write(self._fd, '1' if on else '0')
        self._value = on

    def toggle(self):
        self.set(not self)
        self.sync(None)

    def pulse(self, seconds):
        self.set(True)
        self._timer.start(seconds)

    def _pulse_reset(self):
        self._timer.stop()
        self.set(False)

    def __nonzero__(self):
        self.sync()
        return self._value

    def __str__(self):
        return 'Output(%d)<%s>' % (self._pin, 'ON' if self._value else 'OFF')


class Input(IO):
    def __init__(self, gpio, name, pullup=True, irq='both', active='high', debounce=None):
        
        with open('%s%d/value' % (BASE_PATH, PULLUP_BASE + (gpio - INPUT_BASE)), 'w') as f:
            f.write('1' if pullup else '0')

        with open('%s%d/edge' % (BASE_PATH, gpio), 'w') as f:
            f.write(irq)
            
        with open('%s%d/active_low' % (BASE_PATH, gpio), 'w') as f:
            f.write('0' if active == 'high' else '1')

        super(Input, self).__init__(
            '%s%d/value' % (BASE_PATH, gpio), gpio, name)

        self._debounce       = debounce
        self._debounced_val  = self._value
        self._debounced_last = self._value
        self._triggered      = False

        if debounce:
            self._timer = Timer()
            self._timer.register_callback(self._timeout)

        register(self)

    def triggered(self):
        return self._triggered

    def invalidate(self):
        self._triggered = False

    def depends(self):
        if self._debounce:
            return set([self, self._timer])
        else:
            return super(Input, self).depends()

    def register(self, epoll):
        epoll.register(self._fd, select.EPOLLPRI | select.EPOLLERR)

    def sync(self, event):
        super(Input, self).sync(event)
        if self._debounce:
            self._timer.start(self._debounce)

    def _timeout(self):
        self._timer.stop()
        self._debounced_val = self._value
        if self._debounced_last != self._debounced_val:
            self._debounced_last = self._debounced_val
            self._triggered  = True

    def __nonzero__(self):
        if self._debounce:
            return self._debounced_val
        else:
            return self._value

    def __str__(self):
        return 'Input(%d)<%s>' % (self._pin, 'ON' if self._value else 'OFF')


class Analog(IO):
    def __init__(self, pin, name, vref):

        self._vref = vref

        super(Analog, self).__init__(
            '%s/in_voltage%d_raw' % (ADC_PATH, pin), pin, name)

    def parse_value(self, raw):
        return self._vref * float(raw) / ADC_RAW_MAX

    @property
    def vref(self):
        return self._vref

    def __float__(self):
        self.sync()
        return self._value

    def __str__(self):
        return 'Analog(%d)<%f> [raw: "%s"; vref: %d]' % (self._pin, self._value, self._raw, self._vref)


class HttpError(Exception):
    def __init__(self, code, message):
        super(Exception, self).__init__(message)
        self.code = code

class CgiSocket(Event):
    def __init__(self, path):
        super(CgiSocket, self).__init__()

        try:
            os.unlink(path)
        except OSError:
            if os.path.exists(path):
                raise

        self._socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self._socket.setblocking(0)
        self._socket.bind(path)
        self._socket.listen(0)
        self._cgi   = None
        self._fd    = self._socket.fileno()
        self._valid = False
        register(self)

    def invalidate(self):
        self._valid = False

    def __nonzero__(self):
        return self._valid

    def register(self, epoll):
        epoll.register(self._fd, select.EPOLLIN)

    def sync(self, event):
        self._cgi, _ = self._socket.accept()
        data = self._cgi.recv(512).strip('\n').split('\n')
        
        for var in data:
            name, val = var.split('=', 1)
            if name == 'QUERY_STRING':
                qs = urlparse.parse_qs(val)
                for key in qs.keys():
                    if len(qs[key]) == 1:
                        qs[key] = qs[key][0]

                self._query_string = qs
            elif name == 'PATH_INFO':
                self._url = val.lstrip('/')

        self._valid = True

    def respond(self, status, resp):
        resp = 'Content-type: text/html\r\n' + status + '\r\n\r\n' + resp
        
        try:            
            self._cgi.sendall(resp)
        except socket.error, e:
            if isinstance(e.args, tuple):
                if e[0] == errno.EPIPE:
                    # remote client has disconnected already
                    # ignore the error
                    pass
                else:
                    raise
            else:
                raise
        finally:                
            self._cgi.close()
            self._cgi = None

    def dispatch_callbacks(self):
        for callback in self._callbacks.keys():
            try:
                response = callback(*(self._callbacks[callback] +
                                      (self._url, self._query_string)))
                
                # the first callback which delivers a response wins.
                # see Cgi(On).dispatch later in this file for details
                if response:
                    self.respond('Status: 200 Success', response)
                    return
            
            except HttpError, e:                
                self.respond('Status: ' + str(e.code), e.message)
                return

        self.respond('Status: 404 Not Found', 'No matching callback registered')


class On(object):

    def __init__(self, event):
        self._event = event

    @property
    def event(self):
        return self._event

    def __call__(self, f):
        self.f = f
        return self

    def __get__(self, obj, klass=None):
        if klass is None:
            klass = type(obj)

        def newfunc(*args):
            return self.f(klass, *args)

        return newfunc

    def dispatch(self, this, *args):
        self.f(this, *args)

    def register(self, this):
        self._event.register_callback(self.dispatch, this)

    def unregister(self):
        self._event.unregister_callback(self.dispatch)


class Cgi(On):
    def __init__(self, url):
        super(Cgi, self).__init__(cgi)
        self._url = url.lstrip('/')

    def dispatch(self, this, url, params):
        if self._url != url:
            return False

        try:
            return self.f(this, **params)
        except TypeError:
            return False


class StateMeta(type):
    def __new__(meta, name, bases, attrs):
        state = super(StateMeta, meta).__new__(meta, name, bases, attrs)

        if '__name__' in attrs:
            state.__name__ = attrs['__name__']

        handlers = []
        for base in bases:
            if hasattr(base, '_handlers'):
                handlers += base._handlers

        for key, val in attrs.items():
            if isinstance(val, On):
                handlers.append(val)

        state._handlers = handlers
        return state


class State(object):
    __metaclass__ = StateMeta

    @classmethod
    def enter(cls):
        pass

    @classmethod
    def leave(cls):
        pass

    @classmethod
    def register(cls, epoll):
        for handler in cls._handlers:
            handler.register(cls)

    @classmethod
    def unregister(cls):
        for handler in cls._handlers:
            handler.unregister()

class Udp(object):
    def __init__(self, host, port):
        self._host   = host
        self._port   = port
        if isIpv6(host):
            self._socket = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        else:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    def send(self, message):
        return self._socket.sendto(message, (self._host, self._port))

    def __call__(self, message):
        return self.send(message)


class Tcp(object):
    def __init__(self, host, port):
        if isIpv6(host):
            self._socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        else:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect((host, port))

    def send(self, message):
        self._socket.send(message)

    def __call__(self, message):
        self.send(message)

    def close(self):
        self._socket.close()


class Thermo(Event):
    def __call__(self):
        try:
            W1_SENSORS  = glob.glob(W1_PATH)
            if len(W1_SENSORS):
                with open(W1_SENSORS[0], 'r') as f:
                    temp = f.read()
                    _, temp = temp.split('t=')
                    return int(temp.rstrip()) / 1000.
        except:
            pass
        return 0.0

def register(event):
    EPOLL_EVENTS[event.fileno()] = event
    event.register(EPOLL)

def unregister(event):
    del EPOLL_EVENTS[event.fileno()]
    EPOLL.unregister(event)

def register_event(event):
    EVENTS.append((event, event.depends()))

def unregister_event(event):
    global EVENTS
    EVENTS = [x for x in EVENTS if x[0] != event]

def move(state):
    global CURRENT_STATE
    if CURRENT_STATE is None:
        old = 'None'
    else:
        old = CURRENT_STATE.__name__
        CURRENT_STATE.leave()
        CURRENT_STATE.unregister()

    print 'Transition: %s => %s' % (old, state.__name__)

    CURRENT_STATE = state
    state.enter()
    state.register(EPOLL)

def process_events():
    events = []
    for fileno, event in EPOLL.poll():
        ev = EPOLL_EVENTS[fileno]
        ev.sync(event)
        events.append(ev)

    events = set(events)

    dispatched = []
    for event, deps in EVENTS:
        if (event not in dispatched and
            deps & events and event.triggered() and event):

            event.dispatch_callbacks()
            dispatched.append(event)

    for event in dispatched:
        event.invalidate()


def run(init_state, error_handler=None):
    if not error_handler:
        def error_handler(e, trace):
            print trace

    move(init_state)

    while True:
        try:
            process_events()
        except Exception as e:
            error_handler(e, traceback.format_exc())

cgi = CgiSocket(CGI_SOCKET)
