#!/usr/bin/python

import json
import time
import socket
import datetime
import subprocess

from email import utils
from xml.sax.saxutils import quoteattr
from subprocess import call, check_output

from barionet import plc
from barionet.plc import Timer, State, On, Input, Output, Analog, PingTimeout, Udp, Tcp, Thermo, Cgi
from barionet.config import get_option, set_option

APP_NAME        = 'Barionet 1000'
APP_VERSION     = '1.0.0'
time.sleep(3)   # wait for NTP sync in slow network 
APP_START       = utils.formatdate(time.mktime(datetime.datetime.now().timetuple()), True, False)

# TODO: apply default values if configuration is not provided

TRUE_LIST       = ['true', '1', 'y', 'yes', 'on']

REBOOT_HR       = int(get_option('barionet', 'reboot.time'))
REBOOT_INTERVAL = int(get_option('barionet', 'reboot.interval'))

TEMPERATURE_INTERVAL = int(get_option('barionet', 'app.report_interval')) * 60
SYSLOG_IP       = get_option('barionet', 'app.syslog')
IDS_SERVER1_IP  = get_option('barionet', 'app.server1')
IDS_SERVER2_IP  = get_option('barionet', 'app.server2')
# TODO: move port to the application configuration
IDS_SERVER_PORT = 1468

#
# Status LED
#
LED_BOOT_GPIO   = int(get_option('barionet', 'led_boot.gpio'))
LED_BOOT        = Output(LED_BOOT_GPIO, 'LED - Boot', set_to=True)

#
# Digital inputs
#

DI              = []
DI_COUNT        = int(get_option('barionet', 'gpio.di'))
DI_DELAY        = int(get_option('barionet', 'gpio.debounce_sec'))
for i in xrange(0, DI_COUNT):
    gpio = int(get_option('barionet', 'di%d.gpio' % i))
    name = get_option('barionet', 'di%d.name' % i)
    di_active = get_option('barionet', 'di%d.active' % i).lower()
    di_pullup = get_option('barionet', 'di%d.pullup' % i).lower()
    di = Input(gpio, name, pullup=(di_pullup in TRUE_LIST), active=di_active, debounce=DI_DELAY)
    DI.append(di)

#
# Outputs
#

DO              = []
DO_COUNT        = int(get_option('barionet', 'gpio.do'))
for i in xrange(0, DO_COUNT):
    gpio = int(get_option('barionet', 'do%d.gpio' % i))
    name = get_option('barionet', 'do%d.name' % i)
    
    # NOTE: Last state recovery disabled by the Customer's request 
    # do_enable = int(get_option('barionet', 'do%d.state' % i))
    do_enable = False
    
    DO.append(Output(gpio, name, set_to=do_enable))

#
# Relays
#

RL              = []
RL_COUNT        = int(get_option('barionet', 'gpio.rl'))
for i in xrange(0, RL_COUNT):
    gpio = int(get_option('barionet', 'rl%d.gpio' % i))
    name = get_option('barionet', 'rl%d.name' % i)
    
    # NOTE: Last state recovery disabled by the Customer's request
    # rl_enable = int(get_option('barionet', 'rl%d.state' % i))
    rl_enable = False
    
    RL.append(Output(gpio, name, set_to=rl_enable))

#
# Analog inputs
#

AI              = []
AI_COUNT        = int(get_option('barionet', 'gpio.ai'))
for i in xrange(0, AI_COUNT):
    name = get_option('barionet', 'ai%d.name' % i)
    vref = float(get_option('barionet', 'ai%d.vref' % i))
    AI.append(Analog(i, name, vref))

USB_DEVICE      = [] 
USB_ID          = get_option('barionet', 'usb.id').split()
USB_DISABLED    = get_option('barionet', 'usb.disabled')
for i in xrange(1, 100):
    USB_DEVICE.append('')    

thermo          = Thermo()

syslog = Udp(SYSLOG_IP, 514)

def reboot():
    LED_BOOT.set(True)
    call('/bin/sync', shell = True)
    call('/sbin/reboot', shell = True)
    
def restart_service():
    LED_BOOT.set(True)
    call('/etc/init.d/network restart', shell = True)
    call('/etc/init.d/barionet restart', shell = True)    
    
def sendXML_status():
    try:
        Idle.sendXML_status_Fail = False
        tcp = Tcp(IDS_SERVER1_IP, IDS_SERVER_PORT)
    except socket.error:
#        syslog('sendXML_status failed to connect to %s:%d' % (IDS_SERVER1_IP, IDS_SERVER_PORT))
        Idle.sendXML_status_Fail = True
        return
    xml = '<Substation>' \
       '<Date_Time>%s</Date_Time>' \
       '<Siren_Strobe>%s</Siren_Strobe>' \
       '<Door>%s%s%s</Door>' \
       '<Supply>%s</Supply>' \
       '</Substation>' % (
            datetime.datetime.now().strftime('%y%m%d%H%M%S'),
            '1' if bool(RL[0]) else '0',     # siren  
            '1' if bool(DI[4]) else '0',     # door 1
            '1' if bool(DI[5]) else '0',     # door 2
            '1' if bool(DI[6]) else '0',     # door 3
            '1' if bool(DI[7]) else '0')     # supply 
    tcp(xml)
    tcp.close()
    syslog(xml)
    
def sendXML_temp():
    try:
        Idle.sendXML_temp_Fail = False
        tcp = Tcp(IDS_SERVER1_IP, IDS_SERVER_PORT)
    except socket.error:
#        syslog('sendXML_temp failed to connect to %s:%d' % (IDS_SERVER1_IP, IDS_SERVER_PORT))
        Idle.sendXML_temp_Fail = True
        return
    xml = '<Substation>' \
        '<Date_Time>%s</Date_Time>' \
        '<Temp>%.2f</Temp>' \
        '</Substation>' % (
            datetime.datetime.now().strftime("%y%m%d%H%M%S"),
            thermo()
        )
    tcp(xml)
    tcp.close()
    syslog(xml)

class Idle(State):
    __name__ = 'Idle'

    temp = thermo()
    pingCounter = 0
    tempCounter = 0
    sirenCounter = 0
    routerCounter = 0
    backOn = False
    sendXML_status_Fail = False
    sendXML_temp_Fail = False
    
    @On(Timer(1))
    def read_thermo(state):
        state.temp = '%.2f' % thermo()
        state.tempCounter = (state.tempCounter + 1) % TEMPERATURE_INTERVAL
        if state.tempCounter == 1:
            sendXML_temp()           
     
    @Cgi('/version')
    def cgi_version(state):
        state = {
            'name'    : APP_NAME,
            'version' : APP_VERSION,
            'started' : APP_START,
        };
        return json.dumps(state)
    
    @Cgi('/time')
    def cgi_time(state):
        state = utils.formatdate(time.mktime(datetime.datetime.now().timetuple()), True, False);
        return state
    
    @Cgi('/reboot')
    def cgi_reboot(state):
        syslog('CGI Reboot')
        reboot()    
    
    @Cgi('/restart')
    def cgi_restart(state):
        call('/etc/init.d/barionet restart', shell = True)

    @Cgi('/battery')
    def cgi_bat(state, voltage):
        AI[0]['value'] = float(voltage)/10.0
        
    @Cgi('/state')
    def cgi_state(state):
        
        inputs = []
        for i in xrange(0, len(DI)):
            inputs.append({ 'id' : DI[i].pin, 'value' : bool(DI[i]) })
        
        outputs = []
        for i in xrange(0, len(DO)):
            outputs.append({ 'id' : DO[i].pin, 'value' : bool(DO[i]) })
        
        relays = []
        for i in xrange(0, len(RL)):
            relays.append({ 'id' : RL[i].pin, 'value' : bool(RL[i]) })
        
        analogs = []
        for i in xrange(0, len(AI)):
            analogs.append({ 'id' : AI[i].pin, 'value' : float(AI[i]), 'vref': AI[i].vref })

        state = {
            'temps': [
                { 'name': 'Local', 'value': float(state.temp), 'id': 1 },
            ],
            'analogs': analogs,
            'inputs': inputs,
            'outputs': outputs,
            'relays': relays
        }
        return json.dumps(state)
        
    @Cgi('/')
    def cgi_handler(state, o):        
        num, val = o.split(',')
        num = int(num)
        val = int(val)
        report = 0
        
        # Server will issue "1,1" command to turn-on relay #1
        if num == 1 or num == RL[0].pin:
            num = RL[0].pin
            report = 1
        
        idx = None
        gpio = None
        for i in xrange(0, len(DO)):
            if DO[i].pin == num:
                gpio = DO[i]
                idx = "do%d" % (i)
        
        for i in xrange(0, len(RL)):            
            if RL[i].pin == num:
                gpio = RL[i]
                idx = "rl%d" % (i)
        
        if gpio == None:
            return '<h1>invalid output<h1>'
        
        if val == 0:
            gpio.set(False)
            set_option('barionet', '%s.state=%d' % (idx, 0)) 
        elif val == 1:
            gpio.set(True)
            set_option('barionet', '%s.state=%d' % (idx, 1))
        elif val == 2:
            raise Exception('Hidden exception')
        elif val == 999:
            gpio.toggle()
        else:
            return '<h1>invalid value</h1>'
        
        # if server has requested relay switch with "1,1" command, send back report 
        if report == 1:
            sendXML_status()
        
        return '<h1>OK<h1>'

    # Send XML report on any input change
    @On(DI[0] | ~DI[0] | DI[1] | ~DI[1] | DI[2] | ~DI[2] | DI[3] | ~DI[3] | 
        DI[4] | ~DI[4] | DI[5] | ~DI[5] | DI[6] | ~DI[6] | DI[7] | ~DI[7])
    def input(state):
        sendXML_status()         
    
    # Turn-off Relay 1 after 1 minute
    # Turn-off Relay 2 after 10 seconds
    @On(Timer(1))
    def output(state):
        if RL[0]:
            state.sirenCounter = (state.sirenCounter + 1) % 60
            if state.sirenCounter == 0:
                set_option('barionet', 'rl0.state=%d' % (0)) 
                RL[0].set(False)
                sendXML_status()
        if RL[1]:
            state.routerCounter = (state.routerCounter + 1) % 10
            if state.routerCounter == 0:
                set_option('barionet', 'rl1.state=%d' % (0)) 
                RL[1].set(False)
        
        

    @On(Timer(60))
    def ping(state):	
        state.pingCounter = state.pingCounter % 15 + 1
        syslog('PING Check count is ' + str(state.pingCounter))
        response = call('ping -c 3 -s 8 -q -W 1 ' + IDS_SERVER1_IP, shell = True)        
        if response == 0:
            if state.backOn:
                state.backOn = False
                syslog('Self Reboot after Router back online')  
                restart_service()
            if state.sendXML_status_Fail:
                sendXML_status()    
            if state.sendXML_temp_Fail:
                sendXML_temp()	
        else:            
            if state.pingCounter == 15:
                syslog('PING 1st TIME ' + IDS_SERVER1_IP + ' FAIL')
                response = call('ping -c 2 -s 8 -q -W 1 ' + IDS_SERVER2_IP, shell = True)
                if response != 0:
                    syslog('PING 2nd TIME '+ IDS_SERVER1_IP + ' and ' + IDS_SERVER2_IP  + ' FAIL')
                    syslog('POWER OFF ROUTER')
                    RL[1].set(True)
                    state.backOn = True
            
    @On(Timer(60))
    def softreboot(state):
        rebootTime = datetime.datetime.combine(datetime.date.today(), datetime.time(REBOOT_HR, 00)) + datetime.timedelta(days=REBOOT_INTERVAL)
        if datetime.datetime.now() >= rebootTime:
            syslog('Soft Reboot')
            reboot() 
            
    @On(Timer(5))
    def checkusb(state): 
        if USB_DISABLED != '1':  
            usblist=eval(check_output('ubus -S call usb list', shell = True, universal_newlines=True)) 
            numdev=len(usblist['devices'])-3
            if numdev>0 :
                for d in xrange(0, numdev):    
                    i = int(usblist['devices'][d]['device'])
                    id = usblist['devices'][d]['id']
                    name = usblist['devices'][d]['name']
                    if USB_DEVICE[i] == '':
                        USB_DEVICE[i] = id
                        if id != USB_ID[0] and id != USB_ID[1]:
                            syslog('Unauthorized USB Device plugged: ' + id + ' ' + name)   
              
syslog('%s (v.%s)' % (APP_NAME, APP_VERSION))
syslog('IDS Server IP : ' + IDS_SERVER1_IP)
syslog('COM$ : TCP:' + IDS_SERVER1_IP + ':' + str(IDS_SERVER_PORT))
syslog(APP_START)
sendXML_status()
LED_BOOT.set(False)
plc.run(Idle)
