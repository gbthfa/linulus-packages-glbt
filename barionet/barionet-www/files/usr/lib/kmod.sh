#!/bin/sh

kmod_unload() {
	local modname=$1
	[[ -z $modname ]] && return -1
	
	local dependents=$(lsmod | grep "$modname" | sed -n -e "s/$modname[[:space:]]\+[0-9]\+[[:space:]]\+[0-9]\+[[:space:]]\+\(.*\)/\1/Igp")
	for childmod in $(echo -n "$dependents" | sed 's/,/ /g')
	do
		kmod_unload $childmod
	done
	
	return $(rmmod "$modname")
}
