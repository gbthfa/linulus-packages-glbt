angular.module('app.home', [
  'ui.router'
])
.controller('HomeCtrl', [
    '$scope', '$rootScope', '$state', '$timeout', '$http', 'ubus',
    function ($scope, $rootScope, $state, $timeout, $http, ubus) {

    	$scope.refresh  = true;
    	
    	$scope.temps   = [];
    	$scope.analogs = [];
        $scope.inputs  = [];
        $scope.relays  = [];
        $scope.outputs = [];
        
        $scope.connections = [ { name: 'Guest', server: { ip: '', port: 80 }, client: { ip: '192.168.2.20', port: 9617 }, time: "Nov 12, 2015 @ 18:15:37 GMT+0" } ];

        function clearValues() {
        	for(var i = 0; i < $scope.temps.length; i++)        			
    			$scope.temps[i].value = null;
    		for(var i = 0; i < $scope.inputs.length; i++)
    			$scope.inputs[i].value = null;
    		for(var i = 0; i < $scope.outputs.length; i++)
    			$scope.outputs[i].value = null;
    		for(var i = 0; i < $scope.relays.length; i++)
    			$scope.relays[i].value = null;
    		for(var i = 0; i < $scope.analogs.length; i++)
    			$scope.analogs[i].value = null;
        }
        
        var timeout = false;

        function startLoad() {
            if (!$scope.refresh || timeout)
                return;
            timeout = $timeout(loadState, 2000);
        }

        function stopLoad() {
            $timeout.cancel(timeout);
            timeout = false;
        }
        $scope.$on('$destroy', stopLoad);

        $rootScope.$watch('time', function(newValue, oldValue){
        	
        	// When remote device connection is lost
            if (oldValue != null && newValue == null) {
            	clearValues();
            	// Auto-reload page until successful            	
            	loadState();
            }
            
            // When remote device connection has been has been established 
            if (oldValue == null & newValue != null) {
            	// Update UI state
            	loadState();
            }
        });
        
        function loadState() {
            timeout = false;            
            	
        	// Wait for sensor data to be read... takes time if debouncing delay is enabled
    		$http.get('/rc.cgi/state?'+ new Date().getTime(), { timeout: 7000 })
    		.then(function(res) {

    		  $scope.temps = res.data.temps;

    		  // Rounding temperature to 2 decimal digits and killing unnecessary trailing zeros:
    		  // 0 -> 0
    		  // 24.5 -> 24.5
    		  // 23.768 -> 23.76
          // etc.
    		  for (var i = 0; i < $scope.temps.length; i++)
    		    $scope.temps[i].value = parseFloat(parseFloat($scope.temps[i].value).toFixed(2));
    		  
                
                for(var i = 0; i < res.data.inputs.length; i++) {
                	var elem = _.each($scope.inputs, function(item, key, list) {
	            		if (item.id == this.id) {
	            			item.value = this.value;
	            		}
                    }, res.data.inputs[i]);
                }
                
                for(var i = 0; i < res.data.outputs.length; i++) {
                	var elem = _.each($scope.outputs, function(item, key, list) {
	            		if (item.id == this.id) {
	            			item.value = this.value;
	            		}
                	}, res.data.outputs[i]);
                }
                
                for(var i = 0; i < res.data.relays.length; i++) {
                	var elem = _.each($scope.relays, function(item, key, list) {
	            		if (item.id == this.id) {
	            			item.value = this.value;
	            		}
                	}, res.data.relays[i]);
                }
                
                for(var i = 0; i < res.data.analogs.length; i++) {
                	var elem = _.each($scope.analogs, function(item, key, list) {
	            		if (item.id == this.id) {
	            			item.value = this.value;
	            			
	            			if (item.limit > 0)
                				item.rate = (item.value / item.limit) * 100;
	            		}
                	}, res.data.analogs[i]);
                }
                
            }, function(err) {    		
        		clearValues();
        	})
            .then(startLoad);
        }
        
        $scope.refreshChanged = function(args) {
            stopLoad();
            $scope.refresh = args.val;
            startLoad();
        };

        $scope.outputChanged = function(args) {        	
        	stopLoad();
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
            	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
        		$http.post('/rc.cgi?o=' + args.id + ',' + (+args.val))
            		.then(loadState, loadState);
            });
        };
        
    	// Wait for async authentication checks to complete
    	$rootScope.stateChangeChecks.then(function(res) {
    		
		// Read local configuration (this would be fast)
        ubus.call('uci.get', { config: 'barionet' })
    	.then(function(res) {        		
    		var sections = res.values;
    		
    		var count = sections['gpio'].di;    		
    		for(var i = 0; i < count; i++){
    			var elem = sections['di'+i];
    			$scope.inputs.push({
    				id        : elem.gpio,
    				name      : elem.name,
    				txt_on    : elem.txt_on,
    				txt_off   : elem.txt_off,
    				value     : null,
    			});
    		}
    		
    		count = sections['gpio'].ai;    		
    		for(var i = 0; i < count; i++){
    			var elem = sections['ai'+i];
    			$scope.analogs.push({
    				id        : i,
    				name      : elem.name,
    				limit     : elem.limit,
    				value     : null,
    				ratio     : 0,
            vref      : elem.vref
    			});
    		}
    		
    		count = sections['gpio'].do;    		
    		for(var i = 0; i < count; i++){
    			var elem = sections['do'+i];
    			$scope.outputs.push({
    				id        : elem.gpio,
    				name      : elem.name,
    				txt_on    : elem.txt_on,
    				txt_off   : elem.txt_off,
    				value     : null,
    			});
    		} 
    		
    		count = sections['gpio'].rl;    		
    		for(var i = 0; i < count; i++){
    			var elem = sections['rl'+i];
    			$scope.relays.push({
    				id        : elem.gpio,
    				name      : elem.name,
    				txt_on    : elem.txt_on,
    				txt_off   : elem.txt_off,
    				value     : null,
    			});
    		} 
    	})
    	.then(loadState);
	});
    }
]);
