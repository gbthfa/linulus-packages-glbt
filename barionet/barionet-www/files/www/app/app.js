// Make sure to include the `ui.router` module as a dependency
angular.module('app', [
    'app.home',
    'app.auth',
    'app.settings.app',
    'app.settings.io',
    'app.settings.sys',
    'ngCookies',
    'ui.router'
])
.run([
    '$rootScope', '$state', '$stateParams', '$location', '$cookieStore', '$http', 'ubus', '$q', '$timeout',
    function ($rootScope, $state, $stateParams, $location, $cookieStore, $http, ubus, $q, $timeout) {

        // It's very handy to add references to $state and $stateParams to the $rootScope
        // so that you can access them from any scope within your applications.For example,
        // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
        // to active whenever 'contacts.list' or one of its decendents is active.
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
	    
        // Update current user information
	    $rootScope.currentUser = ubus.currentSession();
        
	    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
	    	var deferred = $q.defer();
	    	
	    	// DEBUG: console.log('ui::$stateChangeStart("' + toState.name + '") - sess: ' + JSON.stringify($rootScope.currentUser));
	    	
	    	// End current session on logout and go back to home page 
	        if (toState.name === 'logout') {
	        	ubus.endSession().then(function() {
	        		$cookieStore.remove('auth');
		        	$http.defaults.headers.common.Authorization = 'Basic ';
		        	
		            // Update current user information
		            $rootScope.currentUser = ubus.currentSession();

		        	// Reject to stop current state transition
		        	deferred.reject();
		        	
		        	$state.go("home", {}, { reload: true });
	        	});
	        	return deferred.promise;
	        }
	        
        	if (toState.name == 'login') {
        		deferred.resolve();
        		return deferred.promise;
        	}
        	
        	$rootScope.returnToState = null;
	        
	        // Authenticate existent user
		    var auth = $cookieStore.get('auth') || {};
	    	ubus.continueSession(auth.user, auth.token).then(function(res) {
	    		
			    // Update current user information
			    var session = ubus.currentSession();
			    $rootScope.currentUser = session;
			    
			    // Append HTTP Authentication Header
			    if (session.loggedIn)
			        $http.defaults.headers.common['Authorization'] = 'Basic ' + session.sessionId;
			    
	    		// Redirect to login page if not logged in and trying to access a restricted page
		        if (toState.authenticate && !session.loggedIn) {
		        	// Reject to stop current state transition
		        	deferred.reject();
		        	
		        	$rootScope.returnToState = toState;
		        	$state.go('login');
		        } else {
		        	// Resolve to continue current state transition
		        	deferred.resolve();
		        }
	    	});
	    	
	    	$rootScope.stateChangeChecks = deferred.promise;
	    	return deferred.promise;
          });
	    
	    $rootScope.$on('$viewContentLoading', function(event, viewConfig){
	    	$rootScope.tickInterval = 5000;

		    var tick = function() {
		    	$http.get('/rc.cgi/time?'+ new Date().getTime(), { timeout: 5000 })
	    		.then(function(res) {
	    			$rootScope.time = new Date(res.data);
	    			$rootScope.tzoffset = $rootScope.time.getTimezoneOffset() / 60 * 100;

	    			// Correct daylight savings effect in angular-js date formatter
	    			var jan = new Date($rootScope.time.getFullYear(), 0, 1);
	    			var jul = new Date($rootScope.time.getFullYear(), 6, 1);
	    		    var stdTimezoneOffset = Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
	    		    
	    			if ($rootScope.time.getTimezoneOffset() < stdTimezoneOffset)
	    				$rootScope.tzoffset += 100;
	    		},
	    		function(err) {
	    			$rootScope.time = null;
	    			$rootScope.tzoffset = 0;
	    		}).then(function(res) {
	    			$rootScope.clockTimeout = $timeout(tick, $rootScope.tickInterval);
	    		});
		    }

		    if (!$rootScope.clockTimeout)
		    	$rootScope.clockTimeout = $timeout(tick, 100);
		    
		    if (!$rootScope.appName)
			    ubus.call('uci.get', { config: 'barionet', section: 'app' })
		    	.then(function(res) {
		    		var cfg = res.values;
		    		$rootScope.appName = cfg.name;
		        });
	    });
    }
])
.provider('jsonRpc', function() {
    var apiEndpoint = '';

    this.setApiEndpoint = function(url) {
        apiEndpoint = url;
    };

    this.$get = ['$http', '$q', function($http, $q) {
        return function jsonRpc(method, args) {
            var defer = $q.defer();

            $http.post(apiEndpoint, {
                jsonrpc: '2.0',
                id: 1,
                method: method,
                params: args || []
            }).then(function(resp) {
                if (resp.data.error) {
                    defer.reject(resp.data.error);
                } else {
                    defer.resolve(resp.data.result);
                }
            });

            return defer.promise;
        };
    }];

})
.provider('ubus', function() {
    this.$get = ['$q', 'jsonRpc', '$cookieStore', 
                 function($q, jsonRpc, $cookieStore) {
        function Ubus() {
            this.clearSession();
        };

        (function() {
            this.call = function(method) {
            	var self = this;
            	
                var defer = $q.defer();
                var args = Array.prototype.slice.call(arguments);
                args.shift();
                
                var sessionId = this._session;
                if (!(typeof args[0]._session === "undefined") && args[0]._session != null) {
                	sessionId = args[0]._session;
                	delete args[0]["_session"];
                }

                var separator = method.lastIndexOf('.');
                var api = method.substring(0, separator);
                method = method.substring(separator + 1);
                
                // DEBUG: console.log('ubus::call("' + api + ':' + method + '") [' + sessionId + '] ' + JSON.stringify(args));
                
                jsonRpc('call', _.union([sessionId], [api, method], args))
                    .then(function(res) {
                        if (res[0] === 0) {
                            defer.resolve(res[1]);
                        } else {
                            defer.reject(res);
                        }
                    }, function(err) {
                    	// DEBUG
                    	console.log('ubus::call("' + api + ':' + method + '") [' + sessionId + '] ' + JSON.stringify(args) + ' - err: ' + JSON.stringify(err));
                    	defer.reject(err);
                    });

                return defer.promise;
            };

            this.currentSession = function() {
                return {
                	sessionId: this._session,
                	userName:  this._userName,
                	loggedIn:  this._loggedIn
                };
            };
            
            this.setSession = function(sessId, userName, loggedIn) {
            	this._session = sessId;
            	this._userName = userName;
            	this._loggedIn = loggedIn;
            	
            	if (sessId != '00000000000000000000000000000000') {            	
	            	// Setup authentication cookies
	        		var auth = {
	        		    user: userName,
	        			token: sessId
	        		};
	        		$cookieStore.put('auth', auth);
            	}
            }
            
            this.clearSession = function() {
            	this.setSession('00000000000000000000000000000000', 'guest', false);
            };

            this.startSession = function(user, passwd) {            	
                var that = this;
                return this.call('session.login', {
                	_session: '00000000000000000000000000000000',                	
                    username: user,
                    password: passwd
                }).then(function(res) {
                	that.setSession(res.ubus_rpc_session, res.data.username, true);
                });
            };
            
            this.continueSession = function(username, sessId) {
            	var self = this;
            	var defer = $q.defer();
            	
            	if(sessId == null || sessId == '' || sessId == '00000000000000000000000000000000') {
            		self.clearSession();
            		defer.resolve(this._session);
            		return defer.promise;
            	}
            	
            	this.call('session.get', {
            		_session: sessId,
            		session : sessId
            	}).then(function(res) {
            		self.setSession(sessId, res.values.username, true);
            		defer.resolve(self._session);
                }, function(err) {                	
                	// Try auto-login with empty password
                	self.startSession(username, "")
                	.then(function(res){
                    	defer.resolve(self._session);
                	}, function(err) {
                		self.clearSession();
                    	defer.resolve(self._session);
                	})
                });
            	
            	return defer.promise;
            }
            
            this.endSession = function() {
            	var self = this;
            	var defer = $q.defer();
            	
            	if (this._loggedIn) {
	            	this.call('session.destroy', {
	                	session : this._session
	                }).then(function(res) {
	                	self.clearSession();
	                	
	                	$cookieStore.remove('auth');
	                	defer.resolve(self._session);
	            	});
            	} else {
            		self.clearSession();
            		
            		$cookieStore.remove('auth');
            		defer.resolve(self._session);
            	}            	
            	
            	return defer.promise;
            };
            
        }).call(Ubus.prototype);
        return new Ubus();
    }];
})
.config([
    '$stateProvider', '$urlRouterProvider', 'jsonRpcProvider',
    function ($stateProvider, $urlRouterProvider, jsonRpcProvider) {
    	$stateProvider
    	.state('login', {
            url: '/login',
            templateUrl: 'app/auth/login.html',
            controller: 'LogInCtrl'
        })
        .state('logout', {
        	cache:false,
            url: '/logout',
        })
        .state('home', {
    	    cache:false,
            url: '/home',
            templateUrl: 'app/home/home.html',
            controller: 'HomeCtrl'
        })
    	.state('settings', {
        	'abstract': true,
        	url: '/settings',
        	template: '<ui-view />',
            authenticate: true
        })
        .state('settings.app', {
        	'abstract': true,
        	url: '/app',
        	template: '<ui-view />',
            authenticate: true
        })
        .state('settings.app.app', {
        	url: '/',
        	templateUrl: 'app/settings/app/settings.app.html',
        	controller: 'SetAppCtrl',
            authenticate: true
        })
        .state('settings.app.net', {
            url: '/net',
            templateUrl: 'app/settings/app/settings.net.html',
            controller: 'SetNetCtrl',
            authenticate: true
        })
        .state('settings.app.wifi', {
            url: '/wifi',
            templateUrl: 'app/settings/app/settings.wifi.html',
            controller: 'SetWifiCtrl',
            authenticate: true
        })
        .state('settings.app.ktcpd', {
          url: '/ktcpd',
          templateUrl: 'app/settings/app/settings.ktcpd.html',
          controller: 'SetKtcpdCtrl',
          authenticate: true
        })
        .state('settings.io', {
        	'abstract': true,
        	url: '/io',
        	template: '<ui-view />',
            authenticate: true
        })
        .state('settings.io.di', {
            url: '/di',
            templateUrl: 'app/settings/io/settings.io.di.html',
            controller: 'SetIoDiCtrl',
            authenticate: true
        })
        .state('settings.io.ai', {
            url: '/ai',
            templateUrl: 'app/settings/io/settings.io.ai.html',
            controller: 'SetIoAiCtrl',
            authenticate: true
        })
        .state('settings.io.do', {
            url: '/do',
            templateUrl: 'app/settings/io/settings.io.do.html',
            controller: 'SetIoDoCtrl',
            authenticate: true
        })
        .state('settings.io.usb', {
            url: '/usb',
            templateUrl: 'app/settings/io/settings.io.usb.html',
            controller: 'SetUsbCtrl',
            authenticate: true
        })
        .state('settings.sys', {
        	'abstract': true,
        	url: '/system',
        	template: '<ui-view />',
            authenticate: true
        })
        
        .state('settings.sys.apps', {
          url: '/apps',
          templateUrl: 'app/settings/system/settings.sys.apps.html',
          controller: 'SetSysAppsCtrl',
          authenticate: true
        })
        .state('settings.sys.time', {
            url: '/time',
            templateUrl: 'app/settings/system/settings.sys.time.html',
            controller: 'SetSysTimeCtrl',
            authenticate: true
        })
        .state('settings.sys.firm', {
            url: '/firm',
            templateUrl: 'app/settings/system/settings.sys.firm.html',
            controller: 'SetSysFirmCtrl',
            authenticate: true
        })
        .state('settings.sys.pswd', {
            url: '/pswd',
            templateUrl: 'app/settings/system/settings.sys.pswd.html',
            controller: 'SetSysPswdCtrl',
            authenticate: true
        })
        .state('settings.sys.rbt', {
            url: '/reboot',
            templateUrl: 'app/settings/system/settings.sys.rbt.html',
            controller: 'SetSysRbtCtrl',
            authenticate: true
        });

        $urlRouterProvider
            .when('/', '/home')
            .otherwise('/');
        jsonRpcProvider.setApiEndpoint('/ubus');
    }
]);
