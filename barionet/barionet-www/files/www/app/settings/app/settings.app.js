angular.module('app.settings.app', [
  'ui.router'
])
.controller('SetAppCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $http, ubus) {
    	var uci_tmz_section = null;
    	
    	$scope.dataLoading = false;
    	$scope.message = null;
		$scope.warning = null;
		$scope.error = null;
		
		$scope.app = {
				name: null,				
				server1: null,
				server2: null,
				syslog: null,
				ntp: null,
				timezone: null,
		};
		
		$scope.reboot_now = {
				notes: null	
		};
		
    	$scope.$on('$destroy', function() { if ($scope.wait_timeout) $timeout.cancel($scope.wait_timeout) });
        function waitForServiceRestart(retryCount) {        	
        	$http.get('/rc.cgi/version?'+ new Date().getTime())
				.then(function() {
					$scope.warning = null;
					$scope.message = "Changes saved. Device is ready.";
					$timeout(function() { $scope.message = null; }, 10000)
					
					$scope.dataLoading = false;
				}, function(err) {
					if (retryCount > 0) {
						retryCount --;
						$scope.warning = "Changes saved. Restarting services... (" + retryCount + ")";
						$scope.wait_timeout = $timeout(waitForServiceRestart, 1000, true, retryCount);
					} else {
						$scope.message = null;
						$scope.warning = null;
						
						$scope.error = "Services are not responding. Try again or reboot the device.";
						$scope.dataLoading = false;
					}
				});
        }
        
        $scope.save = function() {
        	var self = this;
        	$scope.dataLoading = true;        	
        	$scope.message = null;
			$scope.warning = null;
			$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	// Save changes (asynchronously)
	        	ubus.call('uci.set', {
    				config: 'barionet',
    				section: 'app',
    				values: {
	                	name            : $scope.app.name,
	                	report_interval : $scope.app.report_interval, 
	    				server1         : $scope.app.server1,
	    				server2         : $scope.app.server2,
	    				syslog          : $scope.app.syslog
	                },
	            })
	            .then(function(){
	            	ubus.call('uci.commit', {
	                    config: 'barionet'
	                })
		            .then(function(){
		            	ubus.call('uci.set', {
		    				config: 'system',
		    				section: 'ntp',
		    				values: {
		    					server        : $scope.app.ntp
		    				}
			            })
			            .then(function(){
			            	ubus.call('uci.set', {
			    				config: 'system',
			    				section: uci_tmz_section,
			    				values: {
				                	timezone      : $scope.app.timezone
				                },
				            })
				            .then(function(){
				            	ubus.call('uci.commit', {
				                    config: 'system'
				                })
				                .then(function(){
				                	$rootScope.appName = $scope.app.name;
				                	$http.get('/rc.cgi/restart?'+ new Date().getTime());
				                })
				                .then(function(res) {
				        			var retryCount = 25;
				        			$scope.warning = "Changes saved. Restarting services...";
				        			$scope.wait_timeout = $timeout(waitForServiceRestart, 5000, true, retryCount);
				        		}, function(err) {
				        			// DEBUG: 
				        			console.log($state.current + '::save() - err: ' + JSON.stringify(err));
			
				        			$scope.error = "Operation failed with error: " + err.message;
				        			$scope.dataLoading = false;
				        		});
				            });
			            });
		            });
	            });
            });
        };
                
        // Wait for async authentication checks to complete
        $rootScope.stateChangeChecks.then(function(res) {        	
            
	        ubus.call('uci.get', { config: 'system' })
	    	.then(function(res) {
	    		var cfg = res.values;
	    		
	    		$scope.app.ntp = cfg.ntp.server;
	    		$scope.app.timezone = _.find(cfg, function(val, key) {
	    			uci_tmz_section = key;
                    return val.timezone;
                }).timezone;
	        });
	        
	        ubus.call('uci.get', { config: 'barionet', section: 'app' })
	    	.then(function(res) {
	    		var cfg = res.values;
	    		
	    		$scope.app.name = cfg.name;
	    		$scope.app.report_interval = parseInt(cfg.report_interval);
	    		$scope.app.server1 = cfg.server1;
	    		$scope.app.server2 = cfg.server2;
	    		$scope.app.syslog = cfg.syslog;
	        });
        });
    }
])
.controller('SetNetCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $http, ubus) {
      var uci_tmz_section = null;

      $scope.dataLoading = false;
      $scope.message = null;
      $scope.warning = null;
      $scope.error = null;

      $scope.lan = {
        ipaddr: null,
        netmask: null,
        gateway: null,
        dns: [],
        ip6addr: null,
        ip6mask: null,
        ip6gw: null
      };

      var ip6Regex = /([^/]*)(\/(.*))?/g;

      $scope.$on('$destroy', function () { if ($scope.wait_timeout) $timeout.cancel($scope.wait_timeout) });
      function waitForServiceRestart(retryCount) {
        $http.get('/rc.cgi/version?' + new Date().getTime())
          .then(function () {
            $scope.warning = null;
            $scope.message = "Changes saved. Device is ready.";
            $timeout(function () { $scope.message = null; }, 10000)

            $scope.dataLoading = false;
          }, function (err) {

            if (retryCount > 0) {
              retryCount--;
              $scope.warning = "Changes saved. Restarting services... (" + retryCount + ")";
              $scope.wait_timeout = $timeout(waitForServiceRestart, 1000, true, retryCount);
            } else {
              if (err.data == null) {
                $scope.message = "Changes saved. Device is ready.";
                $scope.warning = "Make sure You are using new IP address to access the device.";

                $scope.dataLoading = false;
              } else {
                $scope.message = null;
                $scope.warning = null;

                $scope.error = "Services are not responding. Try again or reboot the device.";
                $scope.dataLoading = false;
              }
            }
          });
      }

      $scope.save = function () {
        var self = this;
        $scope.dataLoading = true;
        $scope.message = null;
        $scope.warning = null;
        $scope.error = null;

        // Refresh session info
        ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
        .then(function (res) {

          // Validate permissions
          if (!ubus.currentSession().loggedIn) {
            $rootScope.returnToState = $state.current;
            $state.go('login');
            return;
          }

          // Save changes (asynchronously)
          var next = ubus.call('uci.delete', { config: 'network', section: 'lan', option: 'ip6addr' });
          next = next.then(function () {
            return ubus.call('uci.delete', { config: 'network', section: 'lan', option: 'ip6gw' });
          });

          next = next.then(function () {
            return ubus.call('uci.set', {
              config: 'network', section: 'lan',
              values: {
                ipaddr: $scope.lan.ipaddr,
                netmask: $scope.lan.netmask,
                gateway: $scope.lan.gateway,
                dns: $scope.lan.dns,
                ip6addr: $scope.lan.ip6addr != null ? ($scope.lan.ip6addr + ($scope.lan.ip6mask != null ? ('/' + $scope.lan.ip6mask) : '')) : null,
                ip6gw: $scope.lan.ip6gw
              },
            });
          });

          next = next.then(function () {
            return ubus.call('uci.commit', { config: 'network' });
          });

          next = next.then(function (res) {
            ubus.call('network.reload', {});
            $http.get('/rc.cgi/restart?' + new Date().getTime());

            var retryCount = 10;
            $scope.warning = "Changes saved. Restarting services...";
            $scope.wait_timeout = $timeout(waitForServiceRestart, 5000, true, retryCount);
          }, function (err) {
            // DEBUG: 
            console.log($state.current + '::save() - err: ' + JSON.stringify(err));

            $scope.error = "Operation failed with error: " + err.message;
            $scope.dataLoading = false;
          });
        });
      };

      // Wait for async authentication checks to complete
      $rootScope.stateChangeChecks.then(function (res) {

        ubus.call('uci.get', { config: 'network', section: 'lan' })
          .then(function (res) {
            var cfg = res.values;

            var ip6match = ip6Regex.exec(cfg.ip6addr);

            $scope.lan.ipaddr = cfg.ipaddr;
            $scope.lan.netmask = cfg.netmask;
            $scope.lan.gateway = cfg.gateway;

            $scope.lan.dns = cfg.dns;
            if ($scope.lan.dns == null) $scope.lan.dns = [];
            for (var i = $scope.lan.dns.length + 1; i <= 2; i++)
              $scope.lan.dns[i - 1] = '';

            $scope.lan.ip6addr = ip6match[1] != "undefined" ? ip6match[1] : '';
            $scope.lan.ip6mask = parseInt(ip6match[3]);
            $scope.lan.ip6gw = cfg.ip6gw;
          });
      });
    }
])
.controller('SetWifiCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $http, ubus) {    	
    	var netmasks = [ "0.0.0.0", "128.0.0.0", "192.0.0.0", "224.0.0.0", "240.0.0.0", "248.0.0.0", "252.0.0.0", "254.0.0.0", "255.0.0.0",
    	                 "255.128.0.0", "255.192.0.0", "255.224.0.0", "255.240.0.0", "255.248.0.0", "255.252.0.0", "255.254.0.0", "255.255.0.0",
    	                 "255.255.128.0", "255.255.192.0", "255.255.224.0", "255.255.240.0", "255.255.248.0", "255.255.252.0", "255.255.254.0", 
    	                 "255.255.255.0", "255.255.255.128", "255.255.255.192", "255.255.255.224","255.255.255.240","255.255.255.248", "255.255.255.252", "255.255.255.254", "255.255.255.255"
    	               ];
    	    
    $scope.wifiLoading = true;
    $scope.dataLoading = false;
    $scope.message = null;
		$scope.warning = null;
		$scope.error = null;
		
		$scope.selectedItem = 0;
		
		$scope.wifi = {
			up          : false,
			isClient    : false,
			ssid        : null,
			ssidOn      : true,
			encryption  : 'none',
			passphrase  : null,
		};
	    
	    $scope.client = {
			ipaddr      : null,
			netmask     : null,
			gateway     : null
		};
		
		$scope.ap = {
			ipaddr      : null,
			netmask     : null,
			gateway     : null,
			dhcp        : {
				isEnabled : false,
				leasetime : null,
				start     : null,
				limit     : null
			}
		};
		
		$scope.curr_ip = {
			ipaddr      : null,
			netmask     : null,
			gateway     : null
		};
		
		$scope.radio= {
			country : null,
			channel : 0,
			protocol: '11gn',
			power   : 18
		};
		
	    // Wait for async authentication checks to complete
        $rootScope.stateChangeChecks.then(function(res) {
        	loadData();
        });
	    
	    function loadData(retryCount) {        	
	        ubus.call('uci.get', { config: 'wireless', section: '@wifi-iface[0]' })
	    	.then(function(res) {
	    		var cfg = res.values;
	    			    		
	    		$scope.wifi.isClient    = (cfg.mode == 'sta');
	    		$scope.wifi.ssid        = cfg.ssid;
	    		$scope.wifi.ssidOn      = cfg.hidden == '1' ? false : true;
	    		$scope.wifi.encryption  = cfg.encryption;
	    		$scope.wifi.passphrase  = cfg.key;
	    		
	    		ubus.call('uci.get', { config: 'network', section: 'wlan0' })
	    		.then(function(res) {
	    			var cfg = res.values;
    					    			
	    			if (cfg.proto == 'static') {
	    				$scope.client.ipaddr        = cfg.ipaddr;
	    				$scope.client.netmask       = cfg.netmask;
	    				$scope.client.gateway       = cfg.gateway;
	    			} else {
	    				$scope.client.ipaddr        = null;
	    				$scope.client.netmask       = null;
	    				$scope.client.gateway       = null;
	    			}
	    			
	    			$scope.ap.ipaddr        = cfg.ipaddr;
    				$scope.ap.netmask       = cfg.netmask;
    				$scope.ap.gateway       = cfg.gateway;
    				
    				$scope.curr_ip.ipaddr   = null;
	        		$scope.curr_ip.netmask  = null;
	        		$scope.curr_ip.gateway  = null;
	        		
	        		ubus.call('uci.get', { config: 'dhcp', section: 'wlan0' })
		    		.then(function(res) {
		    			var cfg = res.values;
		    			
		    			$scope.ap.dhcp.isEnabled     = (cfg.ignore == '1') ? false : true; 
		    			$scope.ap.dhcp.leasetime     = parseInt(cfg.leasetime);
	    				$scope.ap.dhcp.start         = parseInt(cfg.start);
	    				$scope.ap.dhcp.limit         = $scope.ap.dhcp.start + parseInt(cfg.limit);
	    			
		    			ubus.call('network.wireless.status', {})
			        	.then(function(res){		        		
			        		var cfg = res.radio0.config;
			        		
			        		$scope.radio.country  = cfg.country;
			        		$scope.radio.channel  = cfg.channel == 'auto' ? '0' : cfg.channel;
			        		$scope.radio.protocol = cfg.hwmode + (cfg.htmode.toLowerCase() != 'none' ? 'n' : '');
			        		$scope.radio.power    = cfg.txpower.toFixed(0);
			        		
			        		if (!res.radio0.pending) {		        			
			        			$scope.wifi.up             = res.radio0.up;
			        			
			        			if ($scope.wifi.up)
			        				$timeout(waitForIP, 100, true, 10);
			        			else {
			        				$scope.wifiLoading = false;
			        				$scope.dataLoading = false;
			        			}
			        			
			        		} else {
			        			if (--retryCount > 0) {
			        				$timeout(loadData, 1000, true, retryCount);
			        				return;
			        			}
			        			
		        				$scope.wifiLoading = false;
		        				$scope.dataLoading = false;
			        		}
			        	});
		    		});
	    		});
	        });
	    }
    
    	function waitForIP(retryCount) {    		
    		ubus.call('network.interface.wlan0.status', {})
        	.then(function(res){
        		
        		if (res.up == false || res['ipv4-address'] == null || res['ipv4-address'].length == 0) {
        			if (--retryCount > 0) {
        				$timeout(waitForIP, 1000, true, --retryCount);
        				return;
        			}
        			
        			$scope.wifiLoading = false;
    				$scope.dataLoading = false;
        		}
        		
        		var cfg = res['ipv4-address'][0];	        		
        		$scope.curr_ip.ipaddr        = cfg['address'];
        		$scope.curr_ip.netmask       = netmasks[cfg.mask];
        		$scope.curr_ip.gateway       = cfg.gateway;
				
        		$scope.wifiLoading = false;
				$scope.dataLoading = false;
        	});
    	}
        
        $scope.save = function() {
        	var self = this;
        	$scope.dataLoading = true;        	
        	$scope.message = null;
			$scope.warning = null;
			$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	var protocol = $scope.radio.protocol;
	        	var hwmode = protocol;
	        	var htmode = 'NONE';
	        	if (protocol.indexOf('n', protocol.length - 1) !== -1) {
	        		hwmode = protocol.substring(0, protocol.length - 1);
		        	htmode = 'HT20';
	        	}
	        	
	        	// Save changes (asynchronously)
	        	ubus.call('uci.set', {
    				config  : 'wireless',
    				section : '@wifi-iface[0]',
    				values: {
						mode         : $scope.wifi.isClient ? 'sta' : 'ap',
						ssid         : $scope.wifi.ssid,
						hidden       : $scope.wifi.ssidOn ? '0' : '1',
						encryption   : $scope.wifi.encryption,
						key          : $scope.wifi.passphrase,						mac          : $scope.wifi.mac
    				}
				})
				.then(function() {
					if ($scope.wifi.passphrase == null || $scope.wifi.passphrase == "")
						return ubus.call('uci.delete', {
			    				config  : 'wireless',
			    				section : '@wifi-iface[0]',
			    				option  : "key"
							});
				})
				.then(function(){
					ubus.call('uci.set', {
	    				config  : 'wireless',
	    				section : 'radio0',
    					values : {
	    					country  : $scope.radio.country,
	    					channel  : $scope.radio.channel == "0" ? 'auto' : $scope.radio.channel,
	    					hwmode   : hwmode,
	    					htmode   : htmode,
	    		    		txpower  : $scope.radio.power
    					}    					
					})
		            .then(function(){
		            	ubus.call('uci.commit', {
		                    config: 'wireless'
		                })
		                .then(function() {
		                	// For AP mode it is always static
		                	var proto = 'static';		                	

		                	// Default for client is DHCP, unless IP address was explicitly set
		                	var isClient = $scope.wifi.isClient;
		                	if (isClient)
		                		if ($scope.client.ipaddr != null && $scope.client.ipaddr != "")
		                			proto = 'static';
		                		else
		                			proto = 'dhcp';
		                	
		                	ubus.call('uci.set', {
			    				config  : 'network',
			    				section : 'wlan0',
		    					values  : {
			    					proto     : proto,
			    					ipaddr    : (isClient) ? $scope.client.ipaddr : $scope.ap.ipaddr,
			    					netmask   : (isClient) ? $scope.client.netmask : $scope.ap.netmask
		    					}
							})
							.then(function(){
								ubus.call('uci.commit', {
				                    config: 'network'
				                }).then(function() {
				                	var res;
				                	
				                	if (!$scope.ap.dhcp.isEnabled) {				                	
					                	res = ubus.call('uci.set', {
						    				config  : 'dhcp',
						    				section : 'wlan0',
					    					values  : {
					    						ignore      : '1'  
					    					}
										});
				                	} else {
				                		res = ubus.call('uci.delete', {
						    				config  : 'dhcp',
						    				section : 'wlan0',
						    				option  : "ignore"
										}).then(function() {
				                			return ubus.call('uci.set', {
							    				config  : 'dhcp',
							    				section : 'wlan0',
						    					values  : {
						    						ra             : 'server',
						    						leasetime      : $scope.ap.dhcp.leasetime + 'h',
						    						start          : $scope.ap.dhcp.start,
						    						limit          : $scope.ap.dhcp.limit - $scope.ap.dhcp.start
						    					}
				                			});
										});
									}
				                	
				                	res.then(function(){
				                		ubus.call('uci.commit', {
						                    config: 'dhcp'
						                }).then(function(){
						                	$scope.wifiLoading = true;
						                	
						                	ubus.call('network.wireless.down', {});
											ubus.call('network.reload', {});
						                	ubus.call('network.wireless.reload', {});
						                	
						                	if ($scope.wifi.up)
						                		ubus.call('network.wireless.up', {});
						                }).then(function(res) {
						                	$scope.dataLoading = false;
						                	
						                	$scope.message = "Changes saved. Restarting WiFi...";
						                	$timeout(function() { $scope.message = null; }, 10000);
						                	
						                	$timeout(loadData, 3000, true, 20);
						        		}, loadData);
				                	});
				                });
							});
		                });
		            });
				});
            });
        };
        
        $scope.wifiEnable = function(args) {        	

        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	$scope.wifiLoading = true;
            	
            	// Validate permissions
            	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
            	
            	var res;
            	if (args.val) {
            		res = ubus.call('uci.delete', {
        				config  : 'wireless',
        				section : '@wifi-device[0]',
        				option  : "disabled" 
    				});
            	} else {
            		res = ubus.call('uci.set', {
        				config  : 'wireless',
        				section : '@wifi-device[0]',
        				values: {
    						disabled     : '1'
        				}
    				});
            	}
            	
            	res.then(function() {
            		ubus.call('uci.commit', {
	                    config: 'wireless'
	                }).then(function() {
	                	ubus.call('network.wireless.' + (args.val ? 'up' : 'down'), {})
	            			.then(function() { $timeout(loadData, 3000, true, 20); }, loadData);
	                });
            	});
            });
        };
    }
])
.controller('SetKtcpdCtrl', [
  '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
  function ($state, $scope, $rootScope, $timeout, $http, ubus) {
    $scope.dataLoading = false;
    $scope.message = null;
    $scope.warning = null;
    $scope.error = null;

    $scope.ktcpd = {
      enabled: false,
      timeout: null,
      verbose: null,
      logfile: null
    };

    $scope.rs232 = {
      device: null,
      baud: null,
      bits: null,
      parity: null,
      stop: null,
      flow: null,
      modem: null,
      buffer: null,
    };

    $scope.tcp = {
      port: null,
      ipaddr: null
    };

    $scope.encryption = {
      enabled: false,
      cert: null,
      key: null,
      pass: null
    };

    $scope.udp = {
      enabled: false,
      port: null,
      ipaddr: null
    };

    // Wait for async authentication checks to complete
    $rootScope.stateChangeChecks.then(function (res) {
      loadData();
    });

    function loadData(retryCount) {
      ubus.call('uci.get', { config: 'ktcpd' })
        .then(function (res) {
          var tmp = res.values;

          angular.forEach(tmp, function (cfg, section) {
            switch (cfg['.type']) {

              case 'ktcpd':
                $scope.ktcpd.enabled = (cfg.disabled == '1') ? false : true;
                $scope.ktcpd.timeout = parseInt(cfg.timeout);
                break;

              case 'serial':
                $scope.rs232.device = cfg.device;
                $scope.rs232.baud = cfg.baud;
                $scope.rs232.bits = cfg.bits;
                $scope.rs232.parity = cfg.parity;
                $scope.rs232.stop = cfg.stop;
                $scope.rs232.flow = cfg.flow;
                $scope.rs232.modem = (cfg.modem == '1') ? true : false;
                $scope.rs232.buffer = (cfg.buffer == '1') ? true : false;
                break;

              case 'tcp':
                $scope.tcp.port = parseInt(cfg.port);
                $scope.tcp.addr = cfg.addres;
                break;

              case 'encryption':
                $scope.encryption.enabled = (cfg.disabled == '1') ? false : true;
                $scope.encryption.cert = cfg.cert;
                $scope.encryption.key = cfg.key;
                $scope.encryption.pass = cfg.pass;
                break;

              case 'udp':
                $scope.udp.enabled = (cfg.disabled == '1') ? false : true;
                $scope.udp.port = parseInt(cfg.port);
                $scope.udp.addr = cfg.addres;
                break;
            }            
          });
      });
    }

    $scope.save = function () {
      var self = this;
      $scope.dataLoading = true;
      $scope.message = null;
      $scope.warning = null;
      $scope.error = null;

      // Refresh session info
      ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
      .then(function (res) {

        // Validate permissions
        if (!ubus.currentSession().loggedIn) {
          $rootScope.returnToState = $state.current;
          $state.go('login');
          return;
        }
        
        // Save changes (asynchronously)
        var next = ubus.call('uci.set', {
          config: 'ktcpd',
          section: '@ktcpd[0]',
          values: {
            disabled: $scope.ktcpd.enabled ? '0' : '1',
            timeout: $scope.ktcpd.timeout
          }
        });
        
        next = next.then(function() {
          return ubus.call('uci.set', {
            config: 'ktcpd',
            section: '@serial[0]',
            values: {
              baud: $scope.rs232.baud,
              bits: $scope.rs232.bits,
              parity: $scope.rs232.parity,
              stop: $scope.rs232.stop,
              flow: $scope.rs232.flow,
              modem: $scope.rs232.modem ? '1' : '0',
              buffer: $scope.rs232.buffer ? '1' : '0',
            }
          });
        });

        next = next.then(function() {
          return ubus.call('uci.set', {
            config: 'ktcpd',
            section: '@tcp[0]',
            values: {
              port: $scope.tcp.port,
            }
          });
        });

        if ($scope.tcp.addr == null || $scope.tcp.addr == '')
          next = next.then(function() {
            return ubus.call('uci.delete', {
              config: 'ktcpd',
              section: '@tcp[0]',
              option: "addr"
            });
          });
        else 
          next = next.then(function() {
            return ubus.call('uci.set', {
              config: 'ktcpd',
              section: '@tcp[0]',
              values: {
                addr: $scope.tcp.addr
              }
            });
          });
        
        next = next.then(function() {
          return ubus.call('uci.set', {
            config: 'ktcpd',
            section: '@encryption[0]',
            values: {
              disabled: $scope.encryption.enabled ? '0' : '1',
              cert: $scope.encryption.cert,
              key: $scope.encryption.key,
              pass: $scope.encryption.pass,
            }
          });
        });
        
        next = next.then(function() {
          return ubus.call('uci.set', {
            config: 'ktcpd',
            section: '@udp[0]',
            values: {
              disabled: $scope.udp.enabled ? '0' : '1',
              port: $scope.udp.port,
            }
          });
        });

        if ($scope.udp.addr == null || $scope.udp.addr == '')
          next = next.then(function() {
            return ubus.call('uci.delete', {
              config: 'ktcpd',
              section: '@udp[0]',
              option: "addr"
            });
          });
        else 
          next = next.then(function() {
            return ubus.call('uci.set', {
              config: 'ktcpd',
              section: '@udp[0]',
              values: {
                addr: $scope.udp.addr
              }
            });
          });

        next = next.then(function(){
          return ubus.call('uci.commit', {
            config: 'ktcpd'
          });
        });

        next = next.then(function (res) {
          ubus.call('initd.restart', { name: "ktcpd" });

          $scope.message = "Changes saved. Restarting Serial Port Server...";
          $timeout(function () { $scope.message = null; }, 10000);

          $scope.dataLoading = false;
          $timeout(loadData, 3000, true, 20);
        }, function(err) {
          // DEBUG: 
          console.log($state.current + '::save() - err: ' + JSON.stringify(err));

          $scope.error = "Operation failed with error: " + err.message;

          $scope.dataLoading = false;
          $timeout(loadData, 3000, true, 20);
        });

      });
    };
  }
])
.directive('ipv4', function() {
    return {

      // limit usage to argument only
      restrict: 'A',

      // require NgModelController, i.e. require a controller of ngModel directive
      require: 'ngModel',

      // create linking function and pass in our NgModelController as a 4th argument
      link: function(scope, element, attr, ctrl) {

    	    // please note you can name your function & argument anything you like
    	    function customValidator(ngModelValue) {
    	    	
    	    	// Ignore empty input
    	    	if (ngModelValue == null || ngModelValue == "") {
    	    		ctrl.$setValidity('ipv4Validator', true);
    	    		return ngModelValue;    	    	
    	    	}
    	    	
    	    	if (ngModelValue == '0.0.0.0' || ngModelValue == '255.255.255.255' ||
    	    			!ngModelValue.match(/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/))
    	    		ctrl.$setValidity('ipv4Validator', false);
    	    	else
    	    		ctrl.$setValidity('ipv4Validator', true);

    	        return ngModelValue;
    	    }

    	    // we need to add our customValidator function to an array of other(build-in or custom) functions
    	    // I have not notice any performance issues, but it would be worth investigating how much
    	    // effect does this have on the performance of the app
    	    ctrl.$parsers.push(customValidator);
    	    
    	}
    };
})
.directive('ipv6', function() {
    return {

      // limit usage to argument only
      restrict: 'A',

      // require NgModelController, i.e. require a controller of ngModel directive
      require: 'ngModel',

      // create linking function and pass in our NgModelController as a 4th argument
      link: function(scope, element, attr, ctrl) {

    	    // please note you can name your function & argument anything you like
    	    function customValidator(ngModelValue) {
    	    	
    	    	// Ignore empty input
    	    	if (ngModelValue == null || ngModelValue == "") {
    	    		ctrl.$setValidity('ipv6Validator', true);
    	    		return ngModelValue;    	    	
    	    	}
    	    	
    	    	if (!ngModelValue.match(/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\:){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/))
    	    		ctrl.$setValidity('ipv6Validator', false);
    	    	else
    	    		ctrl.$setValidity('ipv6Validator', true);

    	        return ngModelValue;
    	    }

    	    // we need to add our customValidator function to an array of other(build-in or custom) functions
    	    // I have not notice any performance issues, but it would be worth investigating how much
    	    // effect does this have on the performance of the app
    	    ctrl.$parsers.push(customValidator);
    	    
    	}
    };
})
.directive('higherThan', [
  function() {

    var link = function($scope, $element, $attrs, ctrl) {

      var validate = function(viewValue) {
    			
        var comparisonModel = $attrs.higherThan;

        if(!viewValue || !comparisonModel){
          // It's valid because we have nothing to compare against
          ctrl.$setValidity('higherThan', true);
          return viewValue;
        }

        // It's valid if model is lower than the model we're comparing against
        ctrl.$setValidity('higherThan', parseInt(viewValue, 10) > parseInt(comparisonModel, 10) );
        return viewValue;
      };

      ctrl.$parsers.unshift(validate);
      ctrl.$formatters.push(validate);

      $attrs.$observe('higherThan', function(comparisonModel){
        // Whenever the comparison model changes we'll re-validate
        return validate(ctrl.$viewValue);
      });

    };

    return {
      require: 'ngModel',
      link: link
    };

  }
])
.directive('shownValidation', function() {
	// This directive excludes a hidden input element from validation ($addControl issue)
	// http://stackoverflow.com/questions/21575051/implementing-a-directive-to-exclude-a-hidden-input-element-from-validation-add
  return {
  require: '^form',
  restrict: 'A',
  link: function(scope, element, attrs,form) {
    var control;
    
    scope.$watch(attrs.ngShow,function(value){
      if (!control){
        control = form[element.attr("name")];
          }
          if (value == true){
            form.$addControl(control);
            angular.forEach(control.$error, function(validity, validationToken) {
       form.$setValidity(validationToken, !validity, control);
    });
          }else{
             form.$removeControl(control);
          }
        });
      }
    };
});
