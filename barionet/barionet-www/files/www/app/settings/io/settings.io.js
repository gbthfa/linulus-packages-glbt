angular.module('app.settings.io', [
  'ui.router'
])
.controller('SetIoDiCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $http, ubus) {
    	
    	$scope.dataLoading = false;
    	$scope.message = null;
    	$scope.warning = null;
    	$scope.error = null;
    	
    	$scope.$on('$destroy', function() { if ($scope.wait_timeout) $timeout.cancel($scope.wait_timeout) });
        function waitForServiceRestart(retryCount) {        	
        	$http.get('/rc.cgi/version?'+ new Date().getTime())
				.then(function() {
					$scope.warning = null;
					$scope.message = "Changes saved. Device is ready.";
					$timeout(function() { $scope.message = null; }, 10000)
					
					$scope.dataLoading = false;
				}, function(err) {
					if (retryCount > 0) {
						retryCount --;
						$scope.warning = "Changes saved. Restarting services... (" + retryCount + ")";
						$scope.wait_timeout = $timeout(waitForServiceRestart, 1000, true, retryCount);
					} else {
						$scope.message = null;
						$scope.warning = null;
						
						$scope.error = "Services are not responding. Try again or reboot the device.";
						$scope.dataLoading = false;
					}
				});
        }
    	
        $scope.save = function() {
        	var self = this;
        	$scope.dataLoading = true;        	
        	$scope.message = null;
        	$scope.warning = null;
        	$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	// Save changes (asynchronously)
	        	var last_promice = ubus.call('uci.set', {
    				config: 'barionet',
    				section: 'gpio',
    				values: {
    					debounce_sec      : $scope.debounce_delay
	                },
	            }); 

	        	for(var i = 0; i < $scope.inputs.length; i++) {
	        		var elem = $scope.inputs[i];
	        		
	        		var promice = 
	        			ubus.call('uci.set', {
	        				config: 'barionet',
	        				section: 'di'+i,
	        				values: {
			                	name      : elem.name,
			    				txt_on    : elem.txt_on,
			    				txt_off   : elem.txt_off,
			    				active    : elem.active,
			    				pullup    : elem.pullup,
			                },
			            });
	        		
	        		last_promice.then(function() { return promice; });
	        		last_promice = promice;
	        	}
	        	
	        	if (last_promice != null) {
	        		var promice = 
	        			ubus.call('uci.commit', {
		                    config: 'barionet'
		                }).then(function(){
		                	$http.get('/rc.cgi/restart?'+ new Date().getTime());
		                });
	        		last_promice.then(function() { return promice; });
	        		last_promice = promice;
	        	}
	        	
	        	if (last_promice != null)
	        		last_promice.then(function(res) {
	        			var retryCount = 25;
	        			$scope.warning = "Changes saved. Restarting services...";
	        			$scope.wait_timeout = $timeout(waitForServiceRestart, 5000, true, retryCount);
	        		}, function(err) {
	        			// DEBUG: 
	        			console.log($state.current + '::save() - err: ' + JSON.stringify(err));

	        			$scope.error = "Operation failed with error: " + err.message;
	        			$scope.dataLoading = false;
	        		});
            });
        };
	        
        $scope.inputs  = [];
        $scope.debounce_delay = 0;
        
        // Wait for async authentication checks to complete
        $rootScope.stateChangeChecks.then(function(res) {
	        ubus.call('uci.get', {config: 'barionet'}
	    	).then(function(res) {
	    		var sections = res.values;
	    		var di_count = sections['gpio'].di;
	    		
	    		$scope.debounce_delay = parseInt(sections['gpio'].debounce_sec);
	    		
	    		for(var i = 0; i < di_count; i++){
	    			var elem = sections['di'+i];
	    			$scope.inputs.push({
	    				id        : elem.gpio,
	    				name      : elem.name,
	    				txt_on    : elem.txt_on,
	    				txt_off   : elem.txt_off,
	    				active    : elem.active,
	    				pullup    : elem.pullup,
	    				value     : null,
	    			});
	    		}            			
	        });
        });
    }
])
.controller('SetIoDoCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $http, ubus) {
    	
    	$scope.dataLoading = false;
    	$scope.message = null;
    	$scope.warning = null;
    	$scope.error = null;
    	
    	$scope.$on('$destroy', function() { if ($scope.wait_timeout) $timeout.cancel($scope.wait_timeout) });
        function waitForServiceRestart(retryCount) {        	
        	$http.get('/rc.cgi/version?'+ new Date().getTime())
				.then(function() {
					$scope.warning = null;
					$scope.message = "Changes saved. Device is ready.";
					$timeout(function() { $scope.message = null; }, 10000)
					
					$scope.dataLoading = false;
				}, function(err) {
					if (retryCount > 0) {
						retryCount --;
						$scope.warning = "Changes saved. Restarting services... (" + retryCount + ")";
						$scope.wait_timeout = $timeout(waitForServiceRestart, 1000, true, retryCount);
					} else {
						$scope.message = null;
						$scope.warning = null;
						
						$scope.error = "Services are not responding. Try again or reboot the device.";
						$scope.dataLoading = false;
					}
				});
        }
    	
        $scope.save = function() {
        	var self = this;
        	$scope.dataLoading = true;        	
        	$scope.message = null;
        	$scope.warning = null;
        	$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	// Save changes (asynchronously)
	        	var last_promice = null;
	        	for(var i = 0; i < $scope.relays.length; i++) {
	        		var elem = $scope.relays[i];
	        		
	        		var promice = 
	        			ubus.call('uci.set', {
	        				config: 'barionet',
	        				section: 'rl'+i,
	        				values: {
			                	name      : elem.name,
			    				txt_on    : elem.txt_on,
			    				txt_off   : elem.txt_off,
			    				state     : elem.state
			                },
			            });
	        		
	        		if (last_promice != null)
	        			last_promice.then(function() { return promice; });
	        		
	        		last_promice = promice;
	        	}
	        	
	        	for(var i = 0; i < $scope.outputs.length; i++) {
	        		var elem = $scope.outputs[i];
	        		
	        		var promice = 
	        			ubus.call('uci.set', {
	        				config: 'barionet',
	        				section: 'do'+i,
	        				values: {
			                	name      : elem.name,
			    				txt_on    : elem.txt_on,
			    				txt_off   : elem.txt_off,
			    				state     : elem.state
			                },
			            });
	        		
	        		if (last_promice != null)
	        			last_promice.then(function() { return promice; });
	        		
	        		last_promice = promice;
	        	}
	        	
	        	if (last_promice != null) {
	        		var promice = 
	        			ubus.call('uci.commit', {
		                    config: 'barionet'
		                }).then(function(){
		                	$http.get('/rc.cgi/restart?'+ new Date().getTime());
		                });
	        		last_promice.then(function() { return promice; });
	        		last_promice = promice;
	        	}
	        	
	        	if (last_promice != null)
	        		last_promice.then(function(res) {
	        			var retryCount = 25;
	        			$scope.warning = "Changes saved. Restarting services...";
	        			$scope.wait_timeout = $timeout(waitForServiceRestart, 5000, true, retryCount);
	        		}, function(err) {
	        			// DEBUG: 
	        			console.log($state.current + '::save() - err: ' + JSON.stringify(err));

	        			$scope.error = "Operation failed with error: " + err.message;
	        			$scope.dataLoading = false;
	        		});
            });
        };
	        
        $scope.relays  = [];
        $scope.outputs = [];
        
        // Wait for async authentication checks to complete
        $rootScope.stateChangeChecks.then(function(res) {
	        ubus.call('uci.get', {config: 'barionet'}
	    	).then(function(res) {
	    		var sections = res.values;
	    		
	    		var count = sections['gpio'].rl;
	    		for(var i = 0; i < count; i++){
	    			var elem = sections['rl'+i];
	    			$scope.relays.push({
	    				id        : elem.gpio,
	    				name      : elem.name,
	    				txt_on    : elem.txt_on,
	    				txt_off   : elem.txt_off,
	    				state     : elem.state,
	    				value     : null,
	    			});
	    		}
	    		
	    		count = sections['gpio'].do;
	    		for(var i = 0; i < count; i++){
	    			var elem = sections['do'+i];
	    			$scope.outputs.push({
	    				id        : elem.gpio,
	    				name      : elem.name,
	    				txt_on    : elem.txt_on,
	    				txt_off   : elem.txt_off,
	    				state     : elem.state,
	    				value     : null,
	    			});
	    		}
	        });
        });
    }
])
.controller('SetIoAiCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $http, ubus) {
    	
    	$scope.dataLoading = false;
    	$scope.message = null;
    	$scope.warning = null;
    	$scope.error = null;
    	
    	$scope.$on('$destroy', function() { if ($scope.wait_timeout) $timeout.cancel($scope.wait_timeout) });
        function waitForServiceRestart(retryCount) {        	
        	$http.get('/rc.cgi/version?'+ new Date().getTime())
				.then(function() {
					$scope.warning = null;
					$scope.message = "Changes saved. Device is ready.";
					$timeout(function() { $scope.message = null; }, 10000)
					
					$scope.dataLoading = false;
				}, function(err) {
					if (retryCount > 0) {
						retryCount --;
						$scope.warning = "Changes saved. Restarting services... (" + retryCount + ")";
						$scope.wait_timeout = $timeout(waitForServiceRestart, 1000, true, retryCount);
					} else {
						$scope.message = null;
						$scope.warning = null;
						
						$scope.error = "Services are not responding. Try again or reboot the device.";
						$scope.dataLoading = false;
					}
				});
        }
    	
        $scope.save = function() {
        	var self = this;
        	$scope.dataLoading = true;        	
        	$scope.message = null;
        	$scope.warning = null;
        	$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	// Save changes (asynchronously)
	        	var last_promice = null;
	        	for(var i = 0; i < $scope.analogs.length; i++) {
	        		var elem = $scope.analogs[i];
	        		
	        		var promice = 
	        			ubus.call('uci.set', {
	        				config: 'barionet',
	        				section: 'ai'+i,
	        				values: {
			                	name      : elem.name,
			    				limit     : elem.limit.toFixed(2)
			                },
			            });
	        		
	        		if (last_promice != null)
	        			last_promice.then(function() { return promice; });
	        		
	        		last_promice = promice;
	        	}
	        	
	        	if (last_promice != null) {
	        		var promice = 
	        			ubus.call('uci.commit', {
		                    config: 'barionet'
		                }).then(function(){
		                	$http.get('/rc.cgi/restart?'+ new Date().getTime());
		                });
	        		last_promice.then(function() { return promice; });
	        		last_promice = promice;
	        	}
	        	
	        	if (last_promice != null)
	        		last_promice.then(function(res) {
	        			var retryCount = 25;
	        			$scope.warning = "Changes saved. Restarting services...";
	        			$scope.wait_timeout = $timeout(waitForServiceRestart, 5000, true, retryCount);
	        		}, function(err) {
	        			// DEBUG: 
	        			console.log($state.current + '::save() - err: ' + JSON.stringify(err));

	        			$scope.error = "Operation failed with error: " + err.message;
	        			$scope.dataLoading = false;
	        		});
            });
        };
	        
        $scope.analogs = [];
        
        // Wait for async authentication checks to complete
        $rootScope.stateChangeChecks.then(function(res) {
	        ubus.call('uci.get', {config: 'barionet'}
	    	).then(function(res) {
	    		var sections = res.values;
	    		
	    		var count = sections['gpio'].ai;
	    		for(var i = 0; i < count; i++){
	    			var elem = sections['ai'+i];
	    			$scope.analogs.push({
	    				id        : elem.id,
	    				name      : elem.name,
	    				limit     : parseFloat(elem.limit),
	    				value     : null,
	    				vref      : parseFloat(elem.vref),
	    			});
	    		}
	        });
        });
    }
])
.controller('SetUsbCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$window', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $window, $http, ubus) {
      $scope.dataLoading = true;
      $scope.message = null;
      $scope.warning = null;
      $scope.error = null;
    	
      $scope.usb = {
        enabled: false,
        devices: [],
        id: []
      };
        
      // Wait for async authentication checks to complete
      $rootScope.stateChangeChecks.then(function(res) {
        ubus.call('usb.list', {}).then(function (res) {
          $scope.usb.devices = res.devices;

          $scope.error = "";
          $scope.dataLoading = false;
        }, function (err) {
          $scope.error = err;
          $scope.dataLoading = false;
        });
      });

      //////////////////////////////////////////////////////////////////////////////////
      //
      // Added by Robert Phillips (Avanca) on Oct 20, 2016
      //
      var ip6Regex = /([^/]*)(\/(.*))?/g;

      $scope.$on('$destroy', function () { if ($scope.wait_timeout) $timeout.cancel($scope.wait_timeout) });
      function waitForServiceRestart(retryCount) {
        $http.get('/rc.cgi/version?' + new Date().getTime())
          .then(function () {
            $window.location.reload();
          }, function (err) {

            if (retryCount > 0) {
              retryCount--;
              $scope.warning = "Please wait. Starting services... (" + retryCount + ")";
              $scope.wait_timeout = $timeout(waitForServiceRestart, 1000, true, retryCount);
            } else {
              if (err.data == null) {
                $window.location.reload();
              } else {
                $scope.message = null;
                $scope.warning = null;

                $scope.error = "Services are not responding. Try again or reboot the device.";
                $scope.dataLoading = false;
              }
            }
          });
      }

      $scope.save = function () {
        var self = this;
        $scope.dataLoading = true;
        $scope.message = null;
        $scope.warning = null;
        $scope.error = null;

        // Refresh session info
        ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
        .then(function (res) {

          // Validate permissions
          if (!ubus.currentSession().loggedIn) {
            $rootScope.returnToState = $state.current;
            $state.go('login');
            return;
          }

          // Save changes (asynchronously)
          ubus.call('uci.set', {
            config: 'barionet', section: 'usb',
            values: {
              id: $scope.usb.id
            },
          })
          .then(function () {
            return ubus.call('uci.commit', { config: 'barionet' });
          })
          .then(function (res) {
            $http.get('/rc.cgi/restart?' + new Date().getTime());

            var retryCount = 10;
            $scope.warning = "Changes saved. Restarting services...";
            $scope.wait_timeout = $timeout(waitForServiceRestart, 5000, true, retryCount);
          }, function (err) {
            // DEBUG: 
            console.log($state.current + '::save() - err: ' + JSON.stringify(err));

            $scope.error = "Operation failed with error: " + err.message;
            $scope.dataLoading = false;
          });
        });
      };

      $scope.usbEnable = function (args) {
        var self = this;
        $scope.dataLoading = true;
        $scope.message = null;
        $scope.warning = null;
        $scope.error = null;

        // Refresh session info
        ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
        .then(function (res) {

          // Validate permissions
          if (!ubus.currentSession().loggedIn) {
            $rootScope.returnToState = $state.current;
            $state.go('login');
            return;
          }

          var res;
          if (args.val) {
            res = ubus.call('uci.delete', {
              config: 'barionet',
              section: 'usb',
              option: "disabled"
            });
          } else {
            res = ubus.call('uci.set', {
              config: 'barionet',
              section: 'usb',
              values: {
                disabled: '1'
              }
            });
          }

          res.then(function () {
            ubus.call('uci.commit', {
              config: 'barionet'
            })
            .then(function (res) {
              $http.get('/rc.cgi/reboot?' + new Date().getTime());

              var retryCount = 20;
              $scope.warning = (args.val ? "USB enabled." : "USB disabled.") + " Rebooting device...";
              $scope.wait_timeout = $timeout(waitForServiceRestart, 20000, true, retryCount);
            }, function (err) {
              // DEBUG: 
              console.log($state.current + '::save() - err: ' + JSON.stringify(err));

              $scope.error = "Operation failed with error: " + err.message;
              $scope.dataLoading = false;
            });
          });
        });
      };

      // Wait for async authentication checks to complete
      $rootScope.stateChangeChecks.then(function (res) {
        ubus.call('uci.get', { config: 'barionet', section: 'usb' })
          .then(function (res) {
            var cfg = res.values;
            $scope.usb.enabled = (cfg.disabled == '1') ? false : true;
            $scope.usb.showIds = $scope.usb.enabled;
            $scope.usb.id = cfg.id;
            if ($scope.usb.id == null) $scope.usb.id = [];
            for (var i = $scope.usb.id.length + 1; i <= 2; i++)
              $scope.usb.id[i - 1] = '';

            if (!$scope.usb.enabled)
              $scope.warning = "USB disabled";
          });
      });
      //
      //////////////////////////////////////////////////////////////////////////////////
    }
]);