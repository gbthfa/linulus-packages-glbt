angular.module('app.settings.sys', [
  'ui.router'
])
.controller('SetSysAppsCtrl', [
  '$state', '$scope', '$rootScope', '$location', '$timeout', '$http', 'ubus',
  function ($state, $scope, $rootScope, $location, $timeout, $http, ubus) {

    $scope.fileUploaded = false;
    $scope.dataLoading = false;
    $scope.message = null;
    $scope.warning = null;
    $scope.error = null;

    $scope.application = {
      file: null
    };

    $scope.apps = [
                    { name: "Barionet 1000 Dashboard", version: '1.0.0', isEnabled: true, canDisable: false, canRemove: false },
                    { name: "Serial Port Server", version: '2.0.0', isEnabled: true, canDisable: false, canRemove: false },
                    { name: "Intrusion Detection System (IDS)", version: '1.0.0', isEnabled: true, canDisable: false, canRemove: false },
    ];

    function waitForAppInstalling(retryCount) {
      if (retryCount > 0) {
        retryCount--;
        $scope.warning = "Installing application. Please wait... (" + retryCount + ")";
        $scope.wait_timeout = $timeout(waitForAppInstalling, 1000, true, retryCount);
      } else {
        retryCount = 70
        $scope.warning = "Rebooting device...";
        $scope.wait_timeout = $timeout(waitForDeviceReboot, 5000, true, retryCount);
      }
    }

    function waitForDeviceReboot(retryCount) {
      $http.get('/rc.cgi/version?' + new Date().getTime(), { timeout: 5000 })
    .then(function () {
      $scope.warning = null;
      $scope.message = "Device is ready.";
      $timeout(function () { $scope.message = null; }, 10000)

      $scope.fileUploaded = false;
      $scope.dataLoading = false;

      $state.go('home');
    }, function (err) {
      if (retryCount > 0) {
        retryCount--;
        $scope.warning = "Rebooting device... (" + retryCount + ")";
        $scope.wait_timeout = $timeout(waitForDeviceReboot, 1000, true, retryCount);
      } else {
        $scope.message = null;
        $scope.warning = null;

        $scope.error = "Device is not responding.";
        $scope.fileUploaded = false;
        $scope.dataLoading = false;
      }
    });
    }

    $scope.upload = function (files) {
      var self = this;

      $scope.dataLoading = true;
      $scope.fileUploaded = false;

      $scope.message = null;
      $scope.warning = 'Uploading file...';
      $scope.error = null;

      // Refresh session info
      ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
      .then(function (res) {

        // Validate permissions
        if (!ubus.currentSession().loggedIn) {
          $rootScope.returnToState = $state.current;
          $state.go('login');
          return;
        }

        // Upload image
        var fd = new FormData();
        fd.append('file', files[0]);

        $http.post('/cgi-bin/upload.lua', fd, { headers: { 'Content-Type': undefined } })
          .then(function (res) {
            $scope.warning = null;
            $scope.message = "Click 'Install' to proceed";

            $scope.fileUploaded = true;
            $scope.dataLoading = false;
          }, function (err) {
            $scope.warning = null;
            $scope.error = "Invalid application package";

            angular.element("input[type='file']").val(null);

            // DEBUG
            $scope.fileUploaded = false;
            $scope.dataLoading = false;
          });
      });
    };

    $scope.upgrade = function () {
      var self = this;
      $scope.dataLoading = true;
      $scope.message = null;
      $scope.warning = "Installing application. Please wait.";
      $scope.error = null;

      // Refresh session info
      ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
      .then(function (res) {

        // Validate permissions
        if (!ubus.currentSession().loggedIn) {
          $rootScope.returnToState = $state.current;
          $state.go('login');
          return;
        }

        $http.get('/cgi-bin/install.lua')
          .then(function (res) {
            var retryCount = 10;
            $scope.warning = "Installing application. Please wait.";
            $scope.wait_timeout = $timeout(waitForAppInstalling, 1000, true, retryCount);
          }, function (err) {
            $scope.warning = null;
            $scope.error = "Invalid application package";

            angular.element("input[type='file']").val(null);

            $scope.fileUploaded = false;
            $scope.dataLoading = false;
          });
      });
    };

    // Wait for async authentication checks to complete
    $rootScope.stateChangeChecks.then(function (res) {

      ubus.call('system.board', {})
      .then(function (res) {
        var cfg = res;

        $scope.system.model = 'Barionet 1000';
        $scope.system.firmware = cfg.model;
        $scope.system.version = cfg.release.revision;

        ubus.call('system.info', {})
		      .then(function (res) {
		        var cfg = res;

		        $scope.system.memory.total = parseInt(cfg.memory.total);
		        $scope.system.memory.free = parseInt(cfg.memory.free);
		      });
      });
    });
  }
])
.controller('SetSysTimeCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $http, ubus) {
    	var uci_tmz_section = null;
    	
    	$scope.dataLoading = false;
    	$scope.message = null;
		$scope.warning = null;
		$scope.error = null;
		
		$scope.time = {
				timezone : null,
				ntps     : []
		};
		
    	$scope.$on('$destroy', function() { if ($scope.wait_timeout) $timeout.cancel($scope.wait_timeout) });
        function waitForServiceRestart(retryCount) {        	
        	$http.get('/rc.cgi/version?'+ new Date().getTime())
				.then(function() {
					$scope.warning = null;
					$scope.message = "Changes saved. Device is ready.";
					$timeout(function() { $scope.message = null; }, 10000)
					
					$scope.dataLoading = false;
				}, function(err) {
					if (retryCount > 0) {
						retryCount --;
						$scope.warning = "Changes saved. Restarting services... (" + retryCount + ")";
						$scope.wait_timeout = $timeout(waitForServiceRestart, 1000, true, retryCount);
					} else {
						$scope.message = null;
						$scope.warning = null;
						
						$scope.error = "Services are not responding. Try again or reboot the device.";
						$scope.dataLoading = false;
					}
				});
        }
        
        $scope.save = function() {
        	var self = this;
        	$scope.dataLoading = true;        	
        	$scope.message = null;
			$scope.warning = null;
			$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	var enabled = false;
	        	for (var i=0; i < $scope.time.ntps.length; i++) 
	        		enabled |= ($scope.time.ntps[i] != null) && ($scope.time.ntps[i].trim() != '');
	        	
	        	// Save changes (asynchronously)
            	ubus.call('uci.set', {
    				config: 'system',
    				section: 'ntp',
    				values: {
    					enabled       : enabled ? '1' : '0',
    					server        : $scope.time.ntps
    				}
	            })
	            .then(function(){
	            	ubus.call('uci.set', {
	    				config: 'system',
	    				section: uci_tmz_section,
	    				values: {
		                	timezone      : $scope.time.timezone
		                },
		            })
		            .then(function(){
		            	ubus.call('uci.commit', {
		                    config: 'system'
		                })
		                .then(function(){
		                	$http.get('/rc.cgi/restart?'+ new Date().getTime());
		                })
		                .then(function(res) {
		        			var retryCount = 25;
		        			$scope.warning = "Changes saved. Restarting services...";
		        			$scope.wait_timeout = $timeout(waitForServiceRestart, 5000, true, retryCount);
		        		}, function(err) {
		        			// DEBUG: 
		        			console.log($state.current + '::save() - err: ' + JSON.stringify(err));
	
		        			$scope.error = "Operation failed with error: " + err.message;
		        			$scope.dataLoading = false;
		        		});
		            });
	            });
            });
        };
                
        // Wait for async authentication checks to complete
        $rootScope.stateChangeChecks.then(function(res) {        	
            
	        ubus.call('uci.get', { config: 'system' })
	    	.then(function(res) {
	    		var cfg = res.values;
	    		
	    		$scope.time.ntps = cfg.ntp.server;
	    		if ($scope.time.ntps == null) $scope.time.ntps = [];
	    		for(var i = $scope.time.ntps.length + 1; i <= 4; i++)
	    			$scope.time.ntps[i-1] = '';
	    		
	    		$scope.time.timezone = _.find(cfg, function(val, key) {
	    			uci_tmz_section = key;
                    return val.timezone;
                }).timezone;
	        });
        });
    }
])
.controller('SetSysFirmCtrl', [
    '$state', '$scope', '$rootScope', '$location', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $location, $timeout, $http, ubus) {
    	
    	$scope.fileUploaded = false;
    	$scope.dataLoading = false;
    	$scope.message = null;
		$scope.warning = null;
		$scope.error = null;
		
		$scope.system = {
				model       : null,
				firmware    : null,
				version     : null,
				memory      : {
					total   : 0,
					free    : 0
				}
		};
		
		$scope.firmware = {
				password: null,
				file: null				
		};
		
		$scope.reboot_now = {
				notes: null	
		};
    	
		$scope.logs = [ 
		               { version: '1.0.3', user: 'root', client: { ip: '192.168.2.20', port: 9617 }, time: "Nov 12, 2015 @ 18:15:37 GMT+0" },
		               { version: '1.0.0', user: null, client: null, time: "Nov 12, 2015 @ 18:15:37 GMT+0" }
		           ];
		
		function waitForFlashProgramming(retryCount) {
			if (retryCount > 0) {
				retryCount --;
				$scope.warning = "Upgrading firmware. Please wait... (" + retryCount + ")";
				$scope.wait_timeout = $timeout(waitForFlashProgramming, 1000, true, retryCount);
			} else {
				retryCount = 70
				$scope.warning = "Rebooting device...";
				$scope.wait_timeout = $timeout(waitForDeviceReboot, 5000, true, retryCount);
			}
		}
		
        function waitForDeviceReboot(retryCount) {        	
        	$http.get('/rc.cgi/version?'+ new Date().getTime(), { timeout: 5000 })
				.then(function() {
					$scope.warning = null;
					$scope.message = "Device is ready.";
					$timeout(function() { $scope.message = null; }, 10000)
					
					$scope.fileUploaded = false;
					$scope.dataLoading = false;
					
					$state.go('home');
				}, function(err) {
					if (retryCount > 0) {
						retryCount --;
						$scope.warning = "Rebooting device... (" + retryCount + ")";
						$scope.wait_timeout = $timeout(waitForDeviceReboot, 1000, true, retryCount);
					} else {
						$scope.message = null;
						$scope.warning = null;
						
						$scope.error = "Device is not responding.";
						$scope.fileUploaded = false;
						$scope.dataLoading = false;
					}
				});
        }
    	
        $scope.upload = function(files) {
        	var self = this;
        	
        	$scope.dataLoading = true;
        	$scope.fileUploaded = false;
        	
        	$scope.message = null;
        	$scope.warning = 'Uploading file...';
        	$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {

            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	// Upload image
	        	var fd = new FormData();
	        	fd.append('file', files[0]);
	        	
	        	$http.post('/cgi-bin/firmware.lua', fd, { headers: { 'Content-Type' : undefined } })
	        		.then(function(res) {
	        			$scope.warning = null;
	        			$scope.message = "Click 'Upgrade' to proceed";
	        			
	        			$scope.fileUploaded = true;
	        			$scope.dataLoading = false;
	        		}, function(err) {        			
	        			$scope.warning = null;
	        			$scope.error = "Invalid image file";
	        	
	        			angular.element("input[type='file']").val(null);
	        			
	        			// DEBUG
	        			$scope.fileUploaded = false;
	        			$scope.dataLoading = false;
	        		});
            });
        };
        
        $scope.upgrade = function() {
        	var self = this;
        	$scope.dataLoading = true;        	
        	$scope.message = null;
        	$scope.warning = "Upgrading firmware. Please wait.";
			$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	$http.get('/cgi-bin/upgrade.lua')
	        		.then(function(res) {
	        			var retryCount = 56;
	        			$scope.warning = "Upgrading firmware. Please wait.";
	        			$scope.wait_timeout = $timeout(waitForFlashProgramming, 5000, true, retryCount);
	        		}, function(err) {        			
	        			$scope.warning = null;
	        			$scope.error = "Invalid image file";
	        	
	        			angular.element("input[type='file']").val(null);
	        			
	        			$scope.fileUploaded = false;
	        			$scope.dataLoading = false;
	        		});    			
            });
        };
        
        // Wait for async authentication checks to complete
        $rootScope.stateChangeChecks.then(function(res) {
        	
	        ubus.call('system.board', { })
	        .then(function(res) {
	    		var cfg = res;
	    		
	    		$scope.system.model = 'Barionet 1000';
	    		$scope.system.firmware = cfg.model;
	    		$scope.system.version = cfg.release.revision;
	    		
	    		ubus.call('system.info', { })
		        .then(function(res) {
		    		var cfg = res;
		    		
		    		$scope.system.memory.total = parseInt(cfg.memory.total);
		    		$scope.system.memory.free = parseInt(cfg.memory.free);
		        });
	        });
        });
    }
])
.controller('SetSysPswdCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $http, ubus) {
    	
    	$scope.dataLoading = false;
    	$scope.message = null;
		$scope.error = null;
		
		$scope.password = {
				'curr'    : null,
				'new'     : null,				
				'confirm' : null,
		};
        
        $scope.save = function() {
        	var self = this;
        	$scope.dataLoading = true;
        	
        	$scope.message = null;
			$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	// Minimal password requirements	        	
	        	if ($scope.password.new != null &&
	        			$scope.password.new != "" &&
	        			$scope.password.new.length < 6) {	        		
	        		$scope.dataLoading = false;
	        		$scope.error = "New password is too short.";
	        	    return;
	        	}
	        	if (!($scope.password.new === $scope.password.confirm)) {
	        		$scope.dataLoading = false;
	        		$scope.error = "New password confirmation does not match.";
	        	    return;
	        	}	        	
	        	
	        	// Validate current user password            	
        		ubus.call('session.login', {
        			// IMPORTANT: We should test the password as anonymous user
        			_session: '00000000000000000000000000000000',
        			
                    username: $rootScope.currentUser.userName,
                    password: $scope.password.curr || "",
                })
                .then(function(res) {
                	
                	sessid = res.ubus_rpc_session;
                	userName = res.data.username;
                	
                	var passwd = "U6aMy0wojraho";
                	if ($scope.password.new != null && $scope.password.new != "")
                		passwd = CryptoJS.PHP_CRYPT_MD5($scope.password.new, '$1$');

                	ubus.call('uci.get', {
                		_session :  sessid,
                        config   :  'rpcd'
                    })
                    .then(function(vals) {
                        var section;
                        vals = vals.values;
                        _.each(vals, function(val, key) {
                            if (val.username === userName) {
                                section = key;
                            }
                        });

                        ubus.call('uci.set', {
                        	_session :  sessid,
                            config   : 'rpcd',
                            section  : section,
                            values   : { password: passwd }
                        })
                        .then(function() { 
	                        ubus.call('uci.commit', {
	                        	_session :  sessid,
	                            config   : 'rpcd'
	                        }).then(function(){
	                        	
	                        	ubus.call('session.destroy', {
	                        		_session :  sessid,
	        	                	session  :  sessid
	                        	});
	                        	
                        		ubus.endSession()
                        		.then(function() {
	                        		$rootScope.currentUser = ubus.currentSession(); 
	                            	
	                            	$rootScope.returnToState = $state.current;
	            	        		$state.go('login');
	                        	});
	                        });
                        });
                    });
                	                	
                }, function(err) {                	
                	$scope.dataLoading = false;
	        		$scope.error = "Current password does not match.";
                });
            });
        };
    }
])
.controller('SetSysRbtCtrl', [
    '$state', '$scope', '$rootScope', '$timeout', '$http', 'ubus',
    function ($state, $scope, $rootScope, $timeout, $http, ubus) {
    	
    	$scope.dataLoading = false;
    	$scope.message = null;
		$scope.warning = null;
		$scope.error = null;
		
		$scope.reboot = {
				interval: null,
				time: null				
		};
		
		$scope.reboot_now = {
				notes: null	
		};
    	
		$scope.logs = [ 
		               { name: '<scheduled>', client: null, time: "Nov 12, 2015 @ 18:15:37 GMT+0" },
		               { name: 'root', client: { ip: '192.168.2.20', port: 9617 }, time: "Nov 12, 2015 @ 18:15:37 GMT+0" },
		               { name: 'system', client: null, time: "Nov 12, 2015 @ 18:15:37 GMT+0" },
		               { name: 'system', client: null, time: "Nov 12, 2015 @ 18:15:37 GMT+0" },
		           ];
		
    	$scope.$on('$destroy', function() { if ($scope.wait_timeout) $timeout.cancel($scope.wait_timeout) });
        function waitForServiceRestart(retryCount) {        	
        	$http.get('/rc.cgi/version?'+ new Date().getTime())
				.then(function() {
					$scope.warning = null;
					$scope.message = "Changes saved. Device is ready.";
					$timeout(function() { $scope.message = null; }, 10000)
					
					$scope.dataLoading = false;
				}, function(err) {
					if (retryCount > 0) {
						retryCount --;
						$scope.warning = "Changes saved. Restarting services... (" + retryCount + ")";
						$scope.wait_timeout = $timeout(waitForServiceRestart, 1000, true, retryCount);
					} else {
						$scope.message = null;
						$scope.warning = null;
						
						$scope.error = "Services are not responding. Try again or reboot the device.";
						$scope.dataLoading = false;
					}
				});
        }
        function waitForDeviceReboot(retryCount) {        	
        	$http.get('/rc.cgi/version?'+ new Date().getTime())
				.then(function() {
					$scope.warning = null;
					$scope.message = "Device is ready.";
					$timeout(function() { $scope.message = null; }, 10000)
					
					$scope.reboot_now.notes = null;
					if ($scope.form_reboot) 
						$scope.form_reboot.$setPristine();
					
					$scope.dataLoading = false;
				}, function(err) {
					if (retryCount > 0) {
						retryCount --;
						$scope.warning = "Rebooting device... (" + retryCount + ")";
						$scope.wait_timeout = $timeout(waitForServiceRestart, 1000, true, retryCount);
					} else {
						$scope.message = null;
						$scope.warning = null;
						
						$scope.reboot_now.notes = null;
						if ($scope.form_reboot) 
							$scope.form_reboot.$setPristine();
						
						$scope.error = "Device is not responding.";
						$scope.dataLoading = false;
					}
				});
        }
    	
        $scope.reboot = function() {
        	var self = this;
        	$scope.dataLoading = true;        	
        	$scope.message = null;
			$scope.warning = null;
			$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	// Reboot device (asynchronously)
	        	$http.get('/rc.cgi/reboot?'+ new Date().getTime());
	        	
	        	var retryCount = 25;
    			$scope.warning = "Rebooting device...";
    			$scope.wait_timeout = $timeout(waitForDeviceReboot, 5000, true, retryCount);    			
            });
        };
        
        $scope.save = function() {
        	var self = this;
        	$scope.dataLoading = true;        	
        	$scope.message = null;
			$scope.warning = null;
			$scope.error = null;
        	
        	// Refresh session info
            ubus.continueSession($rootScope.currentUser.userName, $rootScope.currentUser.sessionId)
            .then(function(res) {
            	
            	// Validate permissions
	        	if (!ubus.currentSession().loggedIn) {
	        		$rootScope.returnToState = $state.current;
	        		$state.go('login');
	        		return;
	        	}
	        	
	        	// Save changes (asynchronously)
	        	ubus.call('uci.set', {
    				config: 'barionet',
    				section: 'reboot',
    				values: {
	                	interval      : $scope.reboot.interval,
	    				time          : $scope.reboot.time
	                },
	            }).then(function(){
	            	ubus.call('uci.commit', {
	                    config: 'barionet'
	                }).then(function(){
	                	$http.get('/rc.cgi/restart?'+ new Date().getTime());
	                }).then(function(res) {
	        			var retryCount = 25;
	        			$scope.warning = "Changes saved. Restarting services...";
	        			$scope.wait_timeout = $timeout(waitForServiceRestart, 5000, true, retryCount);
	        		}, function(err) {
	        			// DEBUG: 
	        			console.log($state.current + '::save() - err: ' + JSON.stringify(err));

	        			$scope.error = "Operation failed with error: " + err.message;
	        			$scope.dataLoading = false;
	        		});
	            });
            });
        };
        
        // Wait for async authentication checks to complete
        $rootScope.stateChangeChecks.then(function(res) {
	        ubus.call('uci.get', { config: 'barionet', section: 'reboot' }
	    	).then(function(res) {
	    		var cfg = res.values;
	    		
	    		$scope.reboot.interval = parseInt(cfg.interval, 10);
	    		$scope.reboot.time = parseInt(cfg.time, 10);
	        });
        });
    }
])
.directive("compareTo", function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };
 
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
})
.directive('validFile',function(){
  return {
    require:'ngModel',
    link:function(scope,el,attrs,ngModel){
      //change event is fired when file is selected
      el.bind('change',function(){
        scope.$apply(function(){
          ngModel.$setViewValue(el.val());
          ngModel.$render();
        });
      });
    }
  }
});
