angular.module('app.auth', [
  'ngCookies',
  'ui.router'
])
.controller('LogInCtrl', [
    '$state', '$scope', '$rootScope', '$cookieStore', '$http', 'ubus', '$location',
    function($state, $scope, $rootScope, $cookieStore, $http, ubus, $location) {

	    $scope.error    = null;
		$scope.username = '';
	    $scope.password = '';
	    
	    // Reset current session (if any)
	    ubus.endSession();
	    $rootScope.currentUser = ubus.currentSession();
	    $http.defaults.headers.common.Authorization = 'Basic ';
	
    	// NOTE: 'admin' account empty password auto-login check
    	ubus.startSession("admin", "")
    	.then(function(){
    		var sessionInfo = ubus.currentSession();
    		if(sessionInfo.loggedIn)
    			authorizeUser(sessionInfo);
    	});
    	
	    $scope.login = function() {	  
	    	$scope.error = null;
	    	$scope.dataLoading = true;
	    	
	    	ubus.startSession($scope.username, $scope.password).then(function() {
	    		var sessionInfo = ubus.currentSession();
	    		authorizeUser(sessionInfo);
            }, function() {
            	// $scope.error = response.message;
            	$scope.error = "Invalid User Name or Password."
            	$scope.dataLoading = false;
            });
	    };
	    
	    function authorizeUser(sessionInfo){
    		// Update current user information
            $rootScope.currentUser = sessionInfo;
            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionInfo.sessionId; // jshint ignore:line
    		
    		// Redirect to the page requested
    		if ($rootScope.returnToState)
    			$state.go($rootScope.returnToState);
        	else
        		$state.go('home');
	    }
    }
]);