#!/usr/bin/lua

--
-- This script verifies application package and installs it into the system
--

local os = require "os"
local io = require "io"
local nixio = require "nixio"

local key_path = "/etc/barionet.ccrypt"
local image_path = "/tmp/app.tar.gz.cpt"
local tar_path = "/tmp/app.tar.gz"
local keep_settings = ""
local format, gsub, strfind, strlower, strlen = string.format, string.gsub, string.find, string.lower, string.len

--
-- Generates HTTP response 
--
local function http_resp(status, type, content, length)  
  print("Status: " .. status)
  print("Content-Type: " .. type)  
  print("Content-Length: " .. length)
  print("")
  print(content)
end

--
-- Generates HTTP response
--
local function http_json(jsonstr)
  http_resp("200 OK", "application/json", jsonstr, strlen(jsonstr))
end

--
-- Generates HTTP response
--
local function http_bad_request(message)
  http_resp("400 Bad request", "text/plain", message, strlen(message))
end

--
-- Forks and executes system command
--
function fork_exec(command)
  local pid = nixio.fork()
  if pid > 0 then
    return
  elseif pid == 0 then
    -- change to root dir
    nixio.chdir("/")

    -- patch stdin, out, err to /dev/null
    local null = nixio.open("/dev/null", "w+")
    if null then
      nixio.dup(null, nixio.stderr)
      nixio.dup(null, nixio.stdout)
      nixio.dup(null, nixio.stdin)
      if null:fileno() > 2 then
        null:close()
      end
    end

    -- replace with target command
    nixio.exec("/bin/sh", "-c", command)
  end
end

--
-- Verify image and install application
--
if os.execute("ccdecrypt -f -k '" .. key_path .. "' '" .. image_path .. "'; tar -tzf '" .. tar_path .. "' >/dev/null") == 0 then
  http_json(format('{ "image": "%s" }', image_path))
  
  --
  -- Trigger application installation
  --
  fork_exec(format("cd /; tar -xzf %q; reboot", tar_path))

else
  http_bad_request("Invalid package");
end