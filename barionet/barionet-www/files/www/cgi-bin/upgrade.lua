#!/usr/bin/lua

--
-- This script verifies firmware image and triggers system upgrade
--

local os = require "os"
local io = require "io"
local nixio = require "nixio"

local image_path = "/tmp/firmware.bin"
local keep_settings = ""
local format, gsub, strfind, strlower, strlen = string.format, string.gsub, string.find, string.lower, string.len

--
-- Generates HTTP response 
--
local function http_resp(status, type, content, length)  
  print("Status: " .. status)
  print("Content-Type: " .. type)  
  print("Content-Length: " .. length)
  print("")
  print(content)
end

--
-- Generates HTTP response
--
local function http_json(jsonstr)
  http_resp("200 OK", "application/json", jsonstr, strlen(jsonstr))
end

--
-- Generates HTTP response
--
local function http_bad_request(message)
  http_resp("400 Bad request", "text/plain", message, strlen(message))
end

--
-- Forks and executes system command
--
function fork_exec(command)
  local pid = nixio.fork()
  if pid > 0 then
    return
  elseif pid == 0 then
    -- change to root dir
    nixio.chdir("/")

    -- patch stdin, out, err to /dev/null
    local null = nixio.open("/dev/null", "w+")
    if null then
      nixio.dup(null, nixio.stderr)
      nixio.dup(null, nixio.stdout)
      nixio.dup(null, nixio.stdin)
      if null:fileno() > 2 then
        null:close()
      end
    end

    -- replace with target command
    nixio.exec("/bin/sh", "-c", command)
  end
end

--
-- Verify image and report results
--
if os.execute("sysupgrade --test '" .. image_path .. "' >/dev/null") == 0 then
  http_json(format('{ "image": "%s" }', image_path))
  
  --
  -- Trigger system upgrade
  --
  fork_exec(format("killall dropbear uhttpd; sleep 1; /sbin/sysupgrade %s %q", keep_settings, image_path))

else
  http_bad_request("Invalid image");
end