#!/usr/bin/lua

--
-- This script is a minimalistic implementation for multi-part POST request handling.
-- It only handles our specific scenario of the new Firmware binary upload
--

local os = require "os"
local io = require "io"

local image_path = "/tmp/app.tar.gz.cpt"
local format, gsub, strfind, strlower, strlen = string.format, string.gsub, string.find, string.lower, string.len

--
-- Generates HTTP response 
--
local function http_resp(status, type, content, length)  
  print("Status: " .. status)
  print("Content-Type: " .. type)  
  print("Content-Length: " .. length)
  print("")
  print(content)
end

--
-- Generates HTTP response
--
local function http_json(jsonstr)
  http_resp("200 OK", "application/json", jsonstr, strlen(jsonstr))
end

--
-- Generates HTTP response
--
local function http_bad_request(message)
  http_resp("400 Bad request", "text/plain", message, strlen(message))
end

--
-- Generates HTTP response
--
local function http_error(message)
  http_resp("500 Internal Server Error", "text/plain", message, strlen(message))
end

--
-- Validate content type/length
--
local content_type = os.getenv("CONTENT_TYPE") or ''
local content_length = tonumber(os.getenv("CONTENT_LENGTH") or '0')

local MIME_MULTIPART = "multipart/form-data"
if (string.sub(content_type, 1, strlen(MIME_MULTIPART)) ~= MIME_MULTIPART) then
  http_bad_request(format("Content-Type '%s' is not supported.", content_type))
  return
end

if (content_length <= 0) then
  http_bad_request(format("Content-Length '%d' is invalid.", content_length))
  return
end

--
-- Read post body and save file
--
local _,_,boundary = strfind(content_type, "boundary%=(.-)$")
boundary = "--"..(boundary or '').."--"

local content = ''
content = io.read("*line")               -- boundary marker
content_length = content_length - strlen(content) - 2
content = io.read("*line")               -- Content-Disposition
content_length = content_length - strlen(content) - 2
content = io.read("*line")               -- Content-Type:
content_length = content_length - strlen(content) - 2
content = io.read(2)                     -- \r\n
content_length = content_length - 2

local inputf,err =  io.open(image_path, "wb+")
if inputf == nil then
  http_error(format("Cannot create a temporary file.\n%s", err))
  return
end

content_length = content_length - strlen(boundary) - 1
local len = strlen(boundary)
len = content_length % len

if len > 0 then
  content = io.read(len)
  inputf:write(content)
  content_length = content_length - len
end

len = strlen(boundary)
while content_length > 0 and content ~= nil do
  content = io.read(len)
  inputf:write(content)
  content_length = content_length - len
end

inputf:close() 

--
-- Report results
--
http_json(format('{ "image": "%s" }', image_path))