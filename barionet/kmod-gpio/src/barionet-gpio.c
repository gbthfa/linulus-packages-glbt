#include <linux/slab.h>
#include <linux/err.h>
#include <linux/cdev.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/irqdomain.h>
#include <linux/irq.h>
#include <linux/timer.h>
#include <linux/platform_device.h>
#include <linux/gpio/consumer.h>


#define DRIVER_NAME            "barionet-gpio"
#define BARIONET_POLLINT        (HZ/100)
#define BARIONET_MAX_SPEED      3000000
#define BARIONET_NINPUT         8
#define BARIONET_NOUTPUT        16
#define BARIONET_NGPIO          (BARIONET_NINPUT + BARIONET_NOUTPUT)

#define BARIONET_GPIO_LDOUT     69
#define BARIONET_GPIO_LDINP     70
#define	BARIONET_GPIO_MISO      67
#define	BARIONET_GPIO_MOSI      65
#define	BARIONET_GPIO_SCK       66

static unsigned BARIONET_GPIO_LEDS[BARIONET_NINPUT] = { 30, 31, 32, 33, 29, 28, 24, 25 };

struct barionet_gpio {
  int virq;
  unsigned int irq_type;
};

struct barionet {
  u16 outval;
  u16 inval;
  u16 inval_old;
  
  int base;
  struct gpio_chip gpio_chip;
  struct barionet_gpio inputs[BARIONET_NINPUT];

  struct mutex lock;
  struct mutex irq_lock;
  
  struct irq_domain *irq_domain;  
  unsigned int irq_mask;
  struct timer_list poll_timer;

  struct workqueue_struct *wq;
  struct work_struct poll_work;
  
};

static struct barionet *barionet = NULL;


static void barionet_gpio_irq_mask(struct irq_data *data);
static void barionet_gpio_irq_unmask(struct irq_data *data);
static int barionet_gpio_irq_set_type(struct irq_data *data, unsigned int type);
static void barionet_gpio_irq_bus_lock(struct irq_data *data);
static void barionet_gpio_irq_bus_unlock(struct irq_data *data);
static int barionet_gpio_irq_map(struct irq_domain *domain, unsigned int irq, irq_hw_number_t hwirq);
static void barionet_gpio_irq_dispatch(struct barionet *dev, int old_val, int new_val);


static const struct irq_domain_ops barionet_gpio_irq_domain_ops = {
  .map = barionet_gpio_irq_map
};

static struct irq_chip barionet_gpio_irq_chip = {
  .name = DRIVER_NAME,
  .irq_mask = barionet_gpio_irq_mask,
  .irq_unmask = barionet_gpio_irq_unmask,
  .irq_set_type = barionet_gpio_irq_set_type,
  .irq_bus_lock = barionet_gpio_irq_bus_lock,
  .irq_bus_sync_unlock = barionet_gpio_irq_bus_unlock,
};

static void barionet_gpio_toggle_pin(int pin, int delay) {
  gpio_set_value(pin, 1);
  ndelay(delay);
  gpio_set_value(pin, 0);
}

static void barionet_gpio_flush(struct barionet *dev) {
  u16 outval = dev->outval;
  u16 inval = 0;
  int i;

  mutex_lock(&dev->lock);

  gpio_set_value(BARIONET_GPIO_LDINP, 1);
  ndelay(100);
  gpio_set_value(BARIONET_GPIO_LDINP, 0);

  gpio_set_value(BARIONET_GPIO_LDOUT, 0);

  for (i = 15; i >= 0; i--) {
    gpio_set_value(BARIONET_GPIO_SCK, 0);
    gpio_set_value(BARIONET_GPIO_MOSI, (outval >> i) & 1);

    ndelay(100);

    inval |= gpio_get_value(BARIONET_GPIO_MISO) << i;

    gpio_set_value(BARIONET_GPIO_SCK, 1);
    ndelay(100);
  }

  gpio_set_value(BARIONET_GPIO_LDOUT, 1);
  gpio_set_value(BARIONET_GPIO_MOSI, 0);

  mutex_unlock(&dev->lock);

  dev->inval = inval;

  if (dev->inval != dev->inval_old) {
    barionet_gpio_irq_dispatch(dev, dev->inval_old, dev->inval);
    dev->inval_old = dev->inval;
  }
}

static void barionet_gpio_poll(unsigned long arg) {
  struct barionet *dev = (struct barionet*)arg;

  queue_work(dev->wq, &dev->poll_work);
}

static void barionet_gpio_poll_work_handler(struct work_struct *ws) {
  struct barionet *dev = container_of(ws, struct barionet, poll_work);
  
  int i;
  u16 inval;
  struct gpio_desc *desc;

  barionet_gpio_flush(dev);

  inval = dev->inval >> 8;
  for (i = 0; i < BARIONET_NINPUT; ++i, inval >>= 1)
    gpio_set_value(BARIONET_GPIO_LEDS[i], !(inval & 0x01));

  init_timer(&dev->poll_timer);
  dev->poll_timer.expires = jiffies + BARIONET_POLLINT;
  dev->poll_timer.data = (unsigned)dev;
  dev->poll_timer.function = barionet_gpio_poll;
  add_timer(&dev->poll_timer);
}

static struct barionet *gpio_to_barionet(struct gpio_chip *gc) {
  return container_of(gc, struct barionet, gpio_chip);
}

static int barionet_gpio_to_irq(struct gpio_chip *chip, unsigned offset) {
  struct barionet *dev = gpio_to_barionet(chip);

  if (offset >= BARIONET_NGPIO || offset < BARIONET_NOUTPUT) {
    return -1;
  }

  return dev->inputs[offset - BARIONET_NOUTPUT].virq;
}

static int barionet_gpio_get_value(struct gpio_chip *gc, unsigned offset) {
  struct barionet *dev = gpio_to_barionet(gc);
  u16 val = dev->outval;

  barionet_gpio_flush(dev);

  if (offset >= BARIONET_NOUTPUT) {
    val = dev->inval;
    offset -= 8;
  }

  return (val >> offset) & 0x1;
}

static void barionet_gpio_set_value(struct gpio_chip *gc, unsigned offset, int val) {
  struct barionet *dev = gpio_to_barionet(gc);

  /* dev->outbuf[0] = 0x81; */
  /* dev->outbuf[1] = 0x18; */

  if (val)
    dev->outval |= (1 << offset);
  else
    dev->outval &= ~(1 << offset);

  barionet_gpio_flush(dev);
}

static int barionet_gpio_direction_output(struct gpio_chip *gc,
  unsigned offset, int val) {
  return 0;
}

static int barionet_gpio_direction_input(struct gpio_chip *gc, unsigned offset) {
  return 0;
}

static void barionet_gpio_irq_mask(struct irq_data *data) {
  struct barionet *dev = irq_data_get_irq_chip_data(data);
  dev->irq_mask &= ~(1 << data->hwirq);
}

static void barionet_gpio_irq_unmask(struct irq_data *data) {
  struct barionet *dev = irq_data_get_irq_chip_data(data);
  dev->irq_mask |= 1 << data->hwirq;
}

static int barionet_gpio_irq_set_type(struct irq_data *data, unsigned int type) {
  struct barionet *dev = irq_data_get_irq_chip_data(data);

  dev->inputs[data->hwirq].irq_type = type;

  return 0;
}

static void barionet_gpio_irq_dispatch(struct barionet *dev, int old_val, int new_val) {
  int i;
  old_val >>= 8;
  new_val >>= 8;

  for (i = 0; i < BARIONET_NINPUT; ++i) {
    struct barionet_gpio *pin = &dev->inputs[i];

    /* skip masked IRQs */
    if (!(dev->irq_mask & (1 << i)))
      continue;

    if (((pin->irq_type & IRQF_TRIGGER_RISING) &&
      !(old_val & (1 << i)) && (new_val & (1 << i))) ||
      ((pin->irq_type & IRQF_TRIGGER_FALLING) &&
      (old_val & (1 << i)) && !(new_val & (1 << i))) ||
      ((pin->irq_type & IRQF_TRIGGER_HIGH) &&
      (new_val & (1 << i))) ||
      ((pin->irq_type & IRQF_TRIGGER_LOW) &&
      !(new_val & (1 << i)))) {
      local_irq_disable();
      generic_handle_irq(pin->virq);
      local_irq_enable();
    }
  }
}

static void barionet_gpio_irq_bus_lock(struct irq_data *data) {
  struct barionet *dev = irq_data_get_irq_chip_data(data);
  mutex_lock(&dev->irq_lock);
}

static void barionet_gpio_irq_bus_unlock(struct irq_data *data) {
  struct barionet *dev = irq_data_get_irq_chip_data(data);
  mutex_unlock(&dev->irq_lock);
}

static int barionet_gpio_irq_map(struct irq_domain *domain, unsigned int irq,
  irq_hw_number_t hwirq) {
  irq_set_chip_data(irq, domain->host_data);
  irq_set_chip(irq, &barionet_gpio_irq_chip);
  irq_set_chip_and_handler(irq, &barionet_gpio_irq_chip, handle_simple_irq);

#ifdef CONFIG_ARM
  set_irq_flags(irq, IRQF_VALID);
#else
  irq_set_noprobe(irq);
#endif

  return 0;
}

static int barionet_gpio_probe(struct platform_device *pdev) {
  struct barionet *dev;
  char gpio_txt[32];
  unsigned led;
  int ret = 0;
  int i;  

  printk(KERN_DEBUG DRIVER_NAME ": Loading module\n");

  (void)pdev;

  if (gpio_request(BARIONET_GPIO_LDOUT, "load-output") ||
    gpio_direction_output(BARIONET_GPIO_LDOUT, 0) ||
    gpio_request(BARIONET_GPIO_LDINP, "load-input") ||
    gpio_direction_output(BARIONET_GPIO_LDINP, 0) ||
    gpio_request(BARIONET_GPIO_MOSI, "mosi") ||
    gpio_direction_output(BARIONET_GPIO_MOSI, 0) ||
    gpio_request(BARIONET_GPIO_MISO, "miso") ||
    gpio_direction_input(BARIONET_GPIO_MISO) ||
    gpio_request(BARIONET_GPIO_SCK, "sck") ||
    gpio_direction_output(BARIONET_GPIO_SCK, 1)) {
    printk(KERN_DEBUG DRIVER_NAME ": Failed to allocate GPIOs\n");
    return -1;
  }

  for (i = 0; i < BARIONET_NINPUT; ++i) {
    snprintf(gpio_txt, sizeof(gpio_txt), "io_led_%d", i);
    led = BARIONET_GPIO_LEDS[i];
    if (gpio_request(led, gpio_txt) || gpio_direction_output(led, 0)) {
      printk(KERN_DEBUG DRIVER_NAME ": Failed to allocate LEDs GPIOs (#%d: %u)\n", i, led);
      return -1;
    }
  }

  dev = kzalloc(sizeof(*dev), GFP_KERNEL);
  if (!dev) {
    return -ENOMEM;
  }
  barionet = dev;

  mutex_init(&dev->irq_lock);

  dev->irq_domain = irq_domain_add_linear(
    NULL, BARIONET_NINPUT, &barionet_gpio_irq_domain_ops, dev);

  if (!dev->irq_domain) {
    printk(KERN_DEBUG DRIVER_NAME ": Failed to register IRQ domain\n");
    goto err_dev;
  }

  for (i = 0; i < BARIONET_NINPUT; i++)
    dev->inputs[i].virq = irq_create_mapping(dev->irq_domain, i);

  dev->gpio_chip.label = DRIVER_NAME;
  dev->gpio_chip.direction_input = barionet_gpio_direction_input;
  dev->gpio_chip.direction_output = barionet_gpio_direction_output;
  dev->gpio_chip.get = barionet_gpio_get_value;
  dev->gpio_chip.set = barionet_gpio_set_value;
  dev->gpio_chip.to_irq = barionet_gpio_to_irq;
  dev->gpio_chip.base = -1;
  dev->gpio_chip.ngpio = BARIONET_NGPIO;
  dev->gpio_chip.can_sleep = true;
  dev->gpio_chip.owner = THIS_MODULE;

  mutex_init(&dev->lock);

  ret = gpiochip_add(&dev->gpio_chip);
  if (ret) {
    printk(KERN_DEBUG DRIVER_NAME ": Failed to add GPIO chip\n");
    goto err_irq;
  }

  dev->base = dev->gpio_chip.base;

  dev->wq = create_singlethread_workqueue("barionet_gpio_wq");
  if (!dev->wq) {
    printk(KERN_DEBUG DRIVER_NAME ": Failed to create work queue\n");
    goto err_gpio;
  }

  INIT_WORK(&dev->poll_work, barionet_gpio_poll_work_handler);

  barionet_gpio_poll_work_handler(&dev->poll_work);

  printk(KERN_DEBUG DRIVER_NAME ": Loaded (%d Hz)\n", HZ);

  return ret;

err_gpio:
  gpiochip_remove(&dev->gpio_chip);

err_irq:
  for (i = 0; i < BARIONET_NINPUT; i++) {
    irq_dispose_mapping(dev->inputs[i].virq);
  }

  mutex_destroy(&dev->irq_lock);

err_dev:
  kfree(dev);

  return -1;
}

static int barionet_gpio_remove(struct platform_device *pdev) {
  struct barionet *dev = barionet;
  int i;

  (void)pdev;

  del_timer_sync(&dev->poll_timer);
  destroy_workqueue(dev->wq);
  gpiochip_remove(&dev->gpio_chip);
  mutex_destroy(&dev->lock);

  mutex_destroy(&dev->irq_lock);
  for (i = 0; i < BARIONET_NINPUT; i++) {
    irq_dispose_mapping(dev->inputs[i].virq);
  }

  irq_domain_remove(dev->irq_domain);

  kfree(dev);

  for (i = 0; i < BARIONET_NINPUT; ++i)
    gpio_free(BARIONET_GPIO_LEDS[i]);

  gpio_free(BARIONET_GPIO_LDOUT);
  gpio_free(BARIONET_GPIO_LDINP);
  gpio_free(BARIONET_GPIO_MOSI);
  gpio_free(BARIONET_GPIO_MISO);
  gpio_free(BARIONET_GPIO_SCK);

  printk(KERN_DEBUG DRIVER_NAME ": Removed\n");

  return 0;
}

#ifdef MODULE

  static int __init gpio_barionet_gpio_init(void) {
    printk(KERN_DEBUG DRIVER_NAME ": Loading driver\n");
    return barionet_gpio_probe(NULL);
  }

  static void __exit gpio_barionet_gpio_exit(void) {
    printk(KERN_DEBUG DRIVER_NAME ": Removing driver\n");
    barionet_gpio_remove(NULL);
  }

  module_init(gpio_barionet_gpio_init);
  module_exit(gpio_barionet_gpio_exit);

#else

  subsys_initcall(gpio_barionet_gpio_init);

#endif /* MODULE*/

MODULE_DESCRIPTION("Barionet GPIO expander driver");
MODULE_LICENSE("GPL");
